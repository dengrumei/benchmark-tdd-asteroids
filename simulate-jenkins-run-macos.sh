EditorPath='/Applications/Unity/Hub/Editor/2020.2.0b1/Unity.app/Contents/MacOS/Unity'
ProjectPath=$(pwd)
$EditorPath -runTests  -testPlatform editmode -buildPlatform macOS -scriptingBackend Mono2x -architecture ARM64 -projectPath $ProjectPath -testResults $ProjectPath/results.xml -batchmode -logfile $ProjectPath/Editor.log -cleanedLogFile $ProjectPath/Cleaned.log