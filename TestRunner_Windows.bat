set UnityExePath="C:\Program Files\2019.2\2019.2.14f1_49dd4e9fa428\Editor\Unity.exe"

call set UnityExePath=%%UnityExePath:VERSION=!content!%%
echo Tests will be done using: '%UnityExePath%'
%UnityExePath% -runTests -projectPath %~dp0 -testResults %~dp0\results.xml -testPlatform playmode -batchmode
echo Finished running tests
