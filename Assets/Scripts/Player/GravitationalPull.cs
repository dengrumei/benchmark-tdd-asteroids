﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitationalPull : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (GameManager.GameplayActive())
        {
            if (other.CompareTag("Collectible"))
            {
                Vector3 direction = transform.position - other.transform.position;
                other.attachedRigidbody.AddForce(direction * 6.0f, ForceMode.Acceleration);
                if (direction.sqrMagnitude < 0.125f)
                    other.GetComponent<BaseCollectible>()?.Collect();
                Debug.DrawLine(transform.position, other.transform.position, Color.green);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {

    }
}
