﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputController
{ 
    public static float VerticalAxis    => Input.GetAxis("Vertical");
    public static float HorizontalAxis  => Input.GetAxis("Horizontal");
    public static bool  FireTrigger     => Input.GetButtonDown("Fire1");

    public static bool OnGamepad()
    {
        string[] names = Input.GetJoystickNames();
        //for (int x = 0; x < names.Length; x++)
        //    Debug.Log(names[x]);
        return names.Length > 0;
    }

}