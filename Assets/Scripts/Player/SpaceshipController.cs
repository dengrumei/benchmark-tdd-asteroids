﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipController : DamageableObject
{
    public GameObject damageParticles;
    public GameObject spaceshipDebris;
    public GameObject targetingMissle;

    public WeaponList weaponList;

    public Vector2 direction = Vector2.zero;

    bool isColliding = false;

    public enum Weapon
    {
        Basic,
        Laser
    }

    public Weapon currentWeapon = Weapon.Basic;
    private GameObject weaponInstance;


    private void Update()
    {
        Debug.DrawLine(transform.position, transform.position + transform.up, Color.red);
        if (GameManager.GameplayActive())
            Move();

        if (!GameManager.IsPaused && inBulletTime)
        {
            LockOnTarget();
            bulletTimeTimer -= Time.deltaTime;
            if (bulletTimeTimer < 0.0f)
            {
                BulletTime(false);
            }
        }
    }

    public void Move()
    {
        #if !(UNITY_ANDROID || UNITY_IOS)
        float horizontalAxis = InputController.HorizontalAxis;
        float verticalAxis = InputController.VerticalAxis;
        if (!InputController.OnGamepad())
        {
            if (verticalAxis > 0.0f)
                Thrust(verticalAxis);
            if (Mathf.Abs(horizontalAxis) > 0.0f)
                Turn(horizontalAxis);
        }
        else
        {
            Thrust(new Vector2(horizontalAxis, verticalAxis));
        }

        if (InputController.FireTrigger)
            Shoot();
        #endif

        transform.position += (Vector3)direction * Time.deltaTime * 4.0f;
        CalculatePositionOnCamera();
    }

    public void Thrust(float power)
    {
        direction = Vector2.Lerp(direction, transform.up * power, Time.deltaTime * 4.0f);
    }

    public void Thrust(Vector2 analogInput)
    {
        direction = Vector2.Lerp(direction, analogInput, Time.deltaTime * 4.0f);
        transform.up = direction;
        transform.GetChild(0).localEulerAngles = new Vector3(0.0f, Vector2.SignedAngle(direction, analogInput) / 2.0f, 0.0f);
    }

    public void Turn(float delta)
    {
        transform.eulerAngles = new Vector3(0.0f, 0.0f, transform.eulerAngles.z - delta * 150.0f * Time.deltaTime);
        transform.GetChild(0).localEulerAngles = new Vector3(0.0f, -delta * 30.0f, 0.0f);
    }

    public void Shoot(bool onMobile = false)
    {
        switch (currentWeapon)
        {
            case Weapon.Basic:
                ProjectileController projectile = Instantiate(weaponList.weapons[(int)currentWeapon].weaponPrefab, transform.position, Quaternion.identity).GetComponent<ProjectileController>();
                projectile.SetDirection(transform.up);
                break;
            case Weapon.Laser:
                if (weaponInstance != null)
                    break;
                weaponInstance = Instantiate(weaponList.weapons[(int)currentWeapon].weaponPrefab, transform.position, Quaternion.identity);
                weaponInstance.transform.up = transform.up;
                weaponInstance.transform.parent = transform;
                break;
             default:
                Debug.LogError("Invalid weapon state.");
                break;
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isColliding) 
            return;             //preventing multiple collision triggers on the same frame

        if (collision.gameObject.layer == LayerMask.NameToLayer("Asteroids") || collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            if (CameraController.instance)
                CameraController.instance.ScreenShake(0.2f, 0.2f);

            if(collision.gameObject.layer == LayerMask.NameToLayer("Asteroids"))
                collision.gameObject.GetComponent<AsteroidController>().Split();

            Instantiate(spaceshipDebris, transform.position, transform.GetChild(0).rotation);
            Destroy(gameObject);
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isColliding)
            return;             //preventing multiple collision triggers on the same frame

        if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            if (CameraController.instance)
                CameraController.instance.ScreenShake(0.1f, 0.15f);

            collision.gameObject.GetComponent<DamageableObject>().TakeDamage(collision.contacts[0].point, transform.rotation);

            Instantiate(spaceshipDebris, transform.position, transform.GetChild(0).rotation);
            Destroy(gameObject);
        }
    }

    public override void TakeDamage(Vector3 hitPosition, Quaternion hitAngle)
    {
        Instantiate(spaceshipDebris, transform.position, transform.GetChild(0).rotation);
        GameObject particles = Instantiate(damageParticles, hitPosition, hitAngle);
        particles.transform.localScale = transform.localScale == Vector3.one ? transform.localScale : Vector3.one / 3.0f + transform.localScale / 2.0f;
        Destroy(particles, particles.GetComponent<ParticleSystem>().main.duration);
        Destroy(gameObject);
    }


    private void OnDestroy()
    {
        if (GameManager.instance != null)
            GameManager.instance.RespawnShip();
        isColliding = true;
    }

    private void CalculatePositionOnCamera()
    {
        if (Camera.main != null)
        {
            bool warped = false;

            Vector2 positionOnCamera = Camera.main.WorldToViewportPoint(transform.position);
            if (positionOnCamera.x > 1.05f)
            {
                warped = true;
                transform.position = (Vector2)Camera.main.ViewportToWorldPoint(new Vector2(-0.05f, positionOnCamera.y));
            }
            else if (positionOnCamera.x < -0.05f)
            {
                warped = true;
                transform.position = (Vector2)Camera.main.ViewportToWorldPoint(new Vector2(1.05f, positionOnCamera.y));
            }
            else if (positionOnCamera.y > 1.05f)
            {
                warped = true;
                transform.position = (Vector2)Camera.main.ViewportToWorldPoint(new Vector2(positionOnCamera.x, -0.05f));
            }
            else if (positionOnCamera.y < -0.05f)
            {
                warped = true;
                transform.position = (Vector2)Camera.main.ViewportToWorldPoint(new Vector2(positionOnCamera.x, 1.05f));
            }

            if (warped)
            {
                transform.GetChild(0).GetChild(0).GetComponent<EngineTrail>().ClearParticles();
            }
        }
    }

    public void UpdateWeapon(int score)
    {
       
        Weapon weapon = Weapon.Basic;

        if (score >= 10000)
            weapon = Weapon.Laser;

        if (weapon == currentWeapon)
            return;

        /*
        switch(weapon)
        {
            case Weapon.Basic:
                break;
            case Weapon.Laser:
                break;
        }
        */

        currentWeapon = weapon;
        if(PopupManager.instance)
            PopupManager.instance.InstantiatePopup(transform.position, weaponList.weapons[(int)currentWeapon].weaponName.ToUpper());
    }


    public bool canGetBulletTime = true;
    public bool inBulletTime;
    private float bulletTimeTimer;
    private const float BULLETTIME_DURATION = 5.0f;
    public void BulletTime(bool enable)
    {
        if (inBulletTime == enable)
            return;
        GameManager.instance.ClearBulletTimeAsteroids();
        GameManager.instance.inGameMenuController?.ToggleHideableElements(enable);
        if (enable)
            CameraController.instance.ToggleBulletTimeCamera(enable, () => GameManager.instance.SpawnBulletTimeAsteroids());
        else
            CameraController.instance.ToggleBulletTimeCamera(enable, () => OnBulletTimeFinished());

        inBulletTime = enable;
        bulletTimeTimer = BULLETTIME_DURATION;
    }

    public void OnBulletTimeFinished()
    {
        //GameManager.instance.WipeAllAsteroids();
        StartCoroutine(MissleBarrage(FindObjectsOfType<AsteroidController>()));
    }

    IEnumerator MissleBarrage(AsteroidController[] asteroids)
    {
        for(int i = 0; i < asteroids.Length; i++)
        {
            if (asteroids[i])
            {
                Instantiate(targetingMissle, transform.position, transform.rotation).GetComponent<Missle>().LockOn(asteroids[i], transform.position, transform.up * 5.0f);
                yield return new WaitForSeconds(0.1f);
            }
        }
    }

    public void LockOnTarget()
    {
        RaycastHit m_Hit;
        bool hit = Physics.BoxCast(CameraController.instance.cam.ScreenToWorldPoint(Input.mousePosition), Vector3.one, CameraController.instance.cam.transform.forward * 5.0f, out m_Hit);

        if (hit)
        {
            if(m_Hit.collider.gameObject.layer == LayerMask.NameToLayer("BulletTime"))
            {
                m_Hit.collider.gameObject.GetComponent<BulletTimeAsteroidController>().LockOn();
            }
        }
    }
}
