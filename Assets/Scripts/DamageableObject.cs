﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DamageableObject : MonoBehaviour
{
    public abstract void TakeDamage(Vector3 hitPosition, Quaternion hitAngle);
}
