﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAsteroid : MonoBehaviour
{
    public GameObject asteroidDebris;

    private int splitCount = 0;
    private Vector2 direction = Vector2.right;

    virtual protected void Start()
    {
        transform.rotation = Random.rotation;
    }

    public void Move()
    {
        transform.position += (Vector3)direction * Time.deltaTime / transform.localScale.x / 2.0f;
        if (Vector2.Distance(transform.position, Vector2.zero) > 20.0f)
            DestroyImmediate(gameObject);
    }

    public void Split(bool noSpawn = false)
    {
        if (!noSpawn)
        {
            if (splitCount < 2)
                for (int i = 0; i < 2; i++)
                {
                    AsteroidController roid = Instantiate(gameObject, transform.position, Quaternion.identity).GetComponent<AsteroidController>();
                    roid.SetSplitCount(splitCount + 1);
                    roid.SetDirection(new Vector2(Random.Range(-20.0f, 20.0f), Random.Range(-20.0f, 20.0f)));
                }
        }
        GameManager.AddAsteroidToScore(splitCount);
        Instantiate(asteroidDebris, transform.position, transform.GetChild(0).rotation).transform.localScale = transform.localScale;
        Destroy(gameObject);
    }

    public void SetSplitCount(int value)
    {
        splitCount = value;
        transform.localScale = new Vector3(1.0f, 1.0f, 1.0f) / (Mathf.Pow(2, splitCount));
    }

    public int GetSplitCount()
    {
        return splitCount;
    }

    public void SetDirection(Vector2 value)
    {
        direction = (value - (Vector2)transform.position).normalized;
    }

    public Vector2 GetDirection()
    {
        return direction;
    }

}
