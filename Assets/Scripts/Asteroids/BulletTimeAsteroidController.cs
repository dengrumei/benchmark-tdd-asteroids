﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTimeAsteroidController : BaseAsteroid
{
    private Vector2 turnDirection;
    private Rigidbody rb;
    public GameObject crosshair;
    public RectTransform crosshairTransform;

    bool lockedOn = false;

    public bool LockedOn 
    { 
        get => lockedOn; 
        set 
        {
            if (value == lockedOn)
                return;
            if(value)
                SpawnCrosshair();
            lockedOn = value; 
        } 
    }

    protected override void Start()
    {
        base.Start();
        rb = GetComponent<Rigidbody>();
        turnDirection = new Vector3(Random.value, Random.value, Random.value);
        rb.AddTorque(turnDirection * 10.0f, ForceMode.Force);

    }

    // Update is called once per frame
    void Update()
    {
        if(crosshairTransform != null)
            crosshairTransform.position = CameraController.instance.cam.WorldToScreenPoint(transform.position);
    }

    public void LockOn()
    {
        LockedOn = true;
    }

    private void SpawnCrosshair()
    {
        GameObject go = Instantiate(crosshair, GameManager.instance.inGameMenuController.transform);
        crosshairTransform = go.GetComponent<RectTransform>();
        crosshairTransform.position = CameraController.instance.cam.WorldToScreenPoint(transform.position);
        crosshairTransform.sizeDelta *= transform.localScale.x;
    }

    private void OnDestroy()
    {
        if (crosshairTransform)
            Destroy(crosshairTransform.gameObject);
    }
}
