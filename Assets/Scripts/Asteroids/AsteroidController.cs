﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : BaseAsteroid {

    protected override void Start()
    {
        base.Start();
        GetComponent<Rigidbody2D>().AddTorque(Random.Range(-1.0f, 1.0f), ForceMode2D.Impulse);
    }

    private void Update()
    {
        if(GameManager.GameplayActive())
            Move();
    }

}
