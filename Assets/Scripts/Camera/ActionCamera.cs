﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionCamera : MonoBehaviour {

    Vector3 defaultRotation = new Vector3(23.47f, -33.82f, 124.73f);

    Vector3 defaultPosition = new Vector3(0.388f, 0.32f, -0.58f);


    // Use this for initialization
    void Start () {
        transform.rotation = Quaternion.Euler(defaultRotation);
        GetComponent<Camera>().enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
        transform.rotation = Quaternion.Euler(defaultRotation + new Vector3(Mathf.Sin(Time.time * 1.27f), Mathf.Cos(Time.time * 0.84f), Mathf.Sin(Time.time * 1f)) * 5.0f);
        if (GameManager.SpaceshipIsActive())
        {
            transform.position = Vector3.Slerp(transform.position, GameManager.spaceship.transform.position + defaultPosition, Time.deltaTime * 15.0f);
            //transform.LookAt(GameManager.spaceship.transform.position);
        }
    }
}
