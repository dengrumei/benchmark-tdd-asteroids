﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public static CameraController instance;
    public Camera cam;
    public AudioListener listener;

    private int defaultCullingMask;
    private Vector3 fixedRotation;
    private Vector3 fixedPoint;
    private Vector3 offset = Vector3.zero;

    public Vector2 spaceshipPositionOnCamera;

    private bool backgroundEffectsEnabled = false;
    private Material skybox;
    MaterialPropertyBlock skyboxOffset;

    // Use this for initialization
    void Start () {
        if (instance == null)
        {
            instance = this;

            ////Disables Audio Listener warning spam in tests along with TestAudioListener.cs
            AudioListener[] listeners = FindObjectsOfType<AudioListener>();
            foreach(AudioListener listener in listeners)
                if(listener.gameObject != gameObject)
                    DestroyImmediate(listener);
            ////Disables Audio Listener warning spam in tests along with TestAudioListener.cs
        }
        else if (instance != this)
            Destroy(gameObject);

        cam = GetComponent<Camera>();
        listener = GetComponent<AudioListener>();
        defaultCullingMask = cam.cullingMask;
        fixedPoint = transform.position;

        skybox = RenderSettings.skybox;
        if (skybox.name != "Default-Skybox")  //for tests
            backgroundEffectsEnabled = true;
    }
    
    // Update is called once per frame
    void Update () {
        if (GameManager.SpaceshipIsActive() && GameManager.effectsEnabled)
        {
            if(GameManager.spaceship.inBulletTime)
            {
                transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(Mathf.Sin(Time.time * 1.27f), Mathf.Cos(Time.time * 0.84f), Mathf.Sin(Time.time * 1f)) * Time.deltaTime * 3.0f);
            }
            else
            if (backgroundEffectsEnabled)
            {
                spaceshipPositionOnCamera = Camera.main.WorldToViewportPoint(GameManager.spaceship.transform.position);
                RenderSettings.skybox.SetTextureOffset("_MainTex", Vector2.Lerp(RenderSettings.skybox.GetTextureOffset("_MainTex"), spaceshipPositionOnCamera / 10.0f, Time.deltaTime / 5.0f));
            }
        }

    }
    public void ScreenShake(float intensity, float duration)
    {
        StartCoroutine(Shake(intensity, duration));
    }

    public IEnumerator Shake(float intensity, float duration)
    {
        while (duration > 0.0f)
        {
            offset = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)) / 5.0f * intensity;
            transform.position += offset;
            duration -= Time.unscaledDeltaTime;
            yield return null;
        }
        offset = Vector2.zero;
        transform.position = fixedPoint;
    }


    public void ToggleBulletTimeCamera(bool enable, System.Action callback)
    {
        if (perspectiveChange != null)
            StopCoroutine(perspectiveChange);
        perspectiveChange = StartCoroutine(ChangePerspective(enable, callback));
    }

    Coroutine perspectiveChange;
    public IEnumerator ChangePerspective(bool enable, System.Action callback)
    {
        float time = 0.0f;
        float duration = 0.5f;
        Vector3 targetPosition = enable ? GameManager.spaceship.transform.position - GameManager.spaceship.transform.forward : fixedPoint;

        Quaternion targetRotation1 = enable ? GameManager.spaceship.transform.rotation : Quaternion.identity;
        Quaternion targetRotation2 = enable ? GameManager.spaceship.transform.rotation * Quaternion.Euler(-90.0f, 0.0f, 0.0f) : Quaternion.Euler(0.0f, 0.0f, 0.0f);

        if (enable)
            cam.cullingMask = 1 << LayerMask.NameToLayer("BulletTime");
        else
            cam.cullingMask = defaultCullingMask;

        while (time < duration)
        {
            transform.position = Vector3.Lerp(transform.position, targetPosition, time / duration);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation1, time / duration);
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, enable ? 0.15f : 5.0f, time / duration);
            time += Time.deltaTime;
            yield return null;
        }

        time = 0.0f;
        while (time < duration / 2.0f)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation2, time / duration);
            time += Time.deltaTime;
            yield return null;
        }

        transform.position = targetPosition;
        cam.orthographicSize = 5.0f;

        if (callback != null && GameManager.SpaceshipIsActive())
            callback();
    }

    private void OnApplicationQuit()
    {
        //RenderSettings.skybox.SetTextureOffset("_MainTex", Vector2.zero);
    }
}
