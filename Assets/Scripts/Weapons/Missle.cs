﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missle : MonoBehaviour
{
    private AsteroidController target;
    public GameObject explosionParticles;

    private float timer = 0.0f;
    private Vector2 initialPosition;
    private Vector2 poleVector;
    private Vector3 lastPosition;

    public AsteroidController Target { 
        get => target; 
        set => target = value; 
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(target)
        {
            Vector3 direction = target.transform.position - transform.position;
            //transform.position += direction.normalized * Time.deltaTime * 5.0f;
            lastPosition = transform.position;
            transform.position = Bezier(timer, initialPosition, poleVector, target.transform.position);
            if (direction.sqrMagnitude < 1.0f)
                target.Split(true);
            if(transform.position != lastPosition)
                transform.forward = transform.position - lastPosition;
            timer += Time.deltaTime;
        }
        else
        {
            Detonate();
        }
    }

    public void LockOn(AsteroidController asteroid, Vector2 a, Vector2 b)
    {
        Target = asteroid;
        initialPosition = a;
        poleVector = b;
    }

    public Vector2 Bezier(float t, Vector2 a, Vector2 b, Vector2 c)
    {
        var ab = Vector2.Lerp(a, b, t);
        var bc = Vector2.Lerp(b, c, t);
        return Vector2.Lerp(ab, bc, t);
    }


    void Detonate()
    {
        Instantiate(explosionParticles, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
