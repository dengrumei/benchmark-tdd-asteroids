﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseProjectile : MonoBehaviour {

    public bool destroyOnCollision = true;
    public virtual bool IgnoreSplit() => false;
    [HideInInspector]
    public string damageLayer = "Enemy";

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Asteroids"))
        {
            if (CameraController.instance)
                CameraController.instance.ScreenShake(0.1f, 0.15f);

            if (collision.isActiveAndEnabled)
                collision.gameObject.GetComponent<AsteroidController>().Split(IgnoreSplit());

            if(destroyOnCollision)
                Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer(damageLayer))
        {
            if (CameraController.instance)
                CameraController.instance.ScreenShake(0.1f, 0.15f);

            collision.gameObject.GetComponent<DamageableObject>().TakeDamage(collision.contacts[0].point, transform.rotation);

            if (destroyOnCollision)
                Destroy(gameObject);
        }
    }

    public void SetDamageLayer(string layer)
    {
        damageLayer = layer;
    }
}
