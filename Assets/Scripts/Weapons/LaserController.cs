﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserController : BaseProjectile {

    public bool isActive = true;
    public float duration = 0.75f;
    public override bool IgnoreSplit() => true;

    private void Update()
    {
        if (GameManager.GameplayActive())
        {
            if (isActive)
                Expand();
            else
                Shrink();

            duration -= Time.deltaTime;
            isActive &= duration > 0.0f;
        }
    }

    private void Expand()
    {
        if (transform.localScale.y <= 25.0f)
            transform.localScale += Vector3.up * Time.deltaTime * 75.0f;
    }

    private void Shrink()
    {
        if (!isActive)
        {
            //transform.parent = null;
            isActive = false;
        }

        transform.localScale -= Vector3.up * Time.deltaTime * 75.0f;
        transform.position += transform.up * Time.deltaTime * 75.0f;
        if (transform.localScale.y <= 0.0f)
            Destroy(gameObject);
    }
}
