﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RaycastHide : MonoBehaviour
{
    RectTransform rectTransform;
    Canvas canvas;
    public CanvasGroup group;
    bool forceHidden = false;
    // Start is called before the first frame updabe
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        canvas = transform.root.GetComponent<Canvas>();
        group = GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.SpaceshipIsActive())
        {
            if (forceHidden)
            {
                group.alpha = Mathf.Lerp(group.alpha, 0.0f, Time.unscaledDeltaTime * 15);
            }
            else
            {
                Vector2 pos = Camera.main.WorldToScreenPoint(GameManager.spaceship.transform.position);
                if (rectTransform.position.x + rectTransform.rect.min.x * canvas.scaleFactor * 1.5f < pos.x && rectTransform.position.x + rectTransform.rect.max.x * canvas.scaleFactor * 1.5f > pos.x &&
                    rectTransform.position.y + rectTransform.rect.min.y * canvas.scaleFactor * 1.5f < pos.y && rectTransform.position.y + rectTransform.rect.max.y * canvas.scaleFactor * 1.5f > pos.y)
                {
                    group.alpha = Mathf.Lerp(group.alpha, 0.0f, Time.unscaledDeltaTime * 15);
                }
                else
                {
                    group.alpha = Mathf.Lerp(group.alpha, 1.0f, Time.unscaledDeltaTime * 15);
                }
            }
        }
    }

    public void ForceHide(bool enable)
    {
        forceHidden = enable;
    }
}
