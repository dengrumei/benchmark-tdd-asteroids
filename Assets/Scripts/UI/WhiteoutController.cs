﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteoutController : MonoBehaviour
{
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Whiteout()
    {
        animator.enabled = true;
        animator.Play("Whiteout");
    }
}
