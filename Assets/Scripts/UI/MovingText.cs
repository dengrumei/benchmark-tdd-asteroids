﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovingText : MonoBehaviour
{
    private RectTransform rect;
    private Text text;

    private float maxLifetime = 1.5f;
    private float lifetime = 0.0f;

    private Vector2 endPosition;

    // Start is called before the first frame update
    void Start()
    {
        rect = GetComponent<RectTransform>();
        text = GetComponent<Text>();

        float scaleFactor = transform.root.GetComponent<Canvas>().scaleFactor;
        endPosition = rect.anchoredPosition + Vector2.up * 75.0f * transform.root.GetComponent<Canvas>().scaleFactor;
        rect.localScale *= scaleFactor;
    }

    // Update is called once per frame
    void Update()
    { 
        rect.anchoredPosition = Vector2.Lerp(rect.anchoredPosition, endPosition, lifetime);

        if (lifetime > maxLifetime / 2.0f)  //start fading away after half max lifetime
        {
            if (lifetime > maxLifetime)
                Destroy(gameObject); //fade away instead of destroy
            text.color = Color.Lerp(Color.white, new Color(1.0f, 1.0f, 1.0f, 0.0f), (lifetime - maxLifetime / 2.0f) * 2.0f);
        }

        lifetime += Time.deltaTime;
    }
}
