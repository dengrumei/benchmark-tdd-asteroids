﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameMenuController : MonoBehaviour {
    GameObject pauseMenu;
    GameObject pauseMenuButton;
    public Canvas canvas;
    public AudioSource musicController;

    RaycastHide[] hideableInterface;
    WhiteoutController whiteoutController;

#if (UNITY_ANDROID || UNITY_IPHONE)
    GameObject mobileInputCanvas;
#endif

    private bool pauseMenuActive = false;

	// Use this for initialization
	void Start () {
        canvas = GetComponent<Canvas>();
        pauseMenu = transform.GetChild(0).gameObject;
        pauseMenuButton = transform.GetChild(1).gameObject;
        hideableInterface = GetComponentsInChildren<RaycastHide>();
        whiteoutController = GetComponentInChildren<WhiteoutController>();
#if (UNITY_ANDROID || UNITY_IPHONE)
        pauseMenuButton.SetActive(true);
        mobileInputCanvas = GameObject.Find("MobileInputCanvas");
#endif
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown("Cancel"))
            ChangeMenuState(!pauseMenuActive);
    }


    public void ResumeGameplay()
    {
    
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("Scenes/02_Menu");
    }

    public void ChangeMenuState(bool isPaused)
    {
        if (pauseMenuActive == isPaused)
            return;

        if (isPaused)
            musicController.Pause();
        else
            musicController.UnPause();

        pauseMenu.SetActive(isPaused);

#if (UNITY_ANDROID || UNITY_IPHONE)
        pauseMenuButton.SetActive(!isPaused);
        mobileInputCanvas.SetActive(!isPaused);
#endif
        pauseMenuActive = isPaused;
        GameManager.IsPaused = isPaused;
    }

    public void ToggleHideableElements(bool enable)
    {
        foreach(RaycastHide element in hideableInterface)
        {
            element.ForceHide(enable);
        }
    }

    public void TriggerWhiteout()
    {
        whiteoutController.Whiteout();
    }
}
