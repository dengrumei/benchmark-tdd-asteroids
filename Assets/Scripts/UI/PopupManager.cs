﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupManager : MonoBehaviour
{
    public static PopupManager instance;

    public GameObject textPrefab;

    void Start()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    public void InstantiatePopup(Vector2 worldSpacePosition, string text) 
    {
        if (GameManager.effectsEnabled)
        {
            RectTransform rect = Instantiate(textPrefab).GetComponent<RectTransform>();
            rect.anchoredPosition = Camera.main.WorldToScreenPoint(worldSpacePosition);
            rect.SetParent(transform);
            rect.GetComponent<Text>().text = text;
        }
    }
}
