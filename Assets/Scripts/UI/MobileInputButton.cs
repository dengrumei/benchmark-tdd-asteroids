﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MobileInputButton : MonoBehaviour
{
	// Use this for initialization
	void OnEnable () {
        if(GameManager.SpaceshipIsActive())
            GetComponent<Button>().onClick.AddListener(delegate { GameManager.spaceship.Shoot(); });
    }
}
