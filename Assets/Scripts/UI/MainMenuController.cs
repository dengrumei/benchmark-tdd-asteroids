﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Time.timeScale = 1.0f;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadGameplayScene()
    {
        SceneManager.LoadScene("Scenes/03_Gameplay_Intro");
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
}
