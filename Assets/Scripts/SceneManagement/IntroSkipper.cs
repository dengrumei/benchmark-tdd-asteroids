﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroSkipper : MonoBehaviour
{
    void Update()
    {
        //should skip the intro and load the next scene on button presses from keyboard/gamepad
        if (Input.GetButtonDown("Submit") || Input.GetButtonDown("Cancel"))
            Continue();
    }

    public static void Continue()
    {
        Debug.Assert(SceneManager.GetActiveScene().buildIndex + 1 >= SceneManager.sceneCount, "Invalid scene order/count");
        //loads the next scene in build settings after the current one
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
