﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneUnloader : MonoBehaviour
{
    public string sceneName;
    void OnEnable()
    {
        if (GameManager.instance)
        {
            GameManager.instance.StartCoroutine(Unload());
        }
    }

    public IEnumerator Unload()
    {
        yield return SceneManager.UnloadSceneAsync(sceneName);
        GameManager.instance.BeginBossBattle();
    }
}
