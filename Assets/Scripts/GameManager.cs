﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    /// <summary>
    ///Values and methods for testing
    /// </summary>
    public static bool effectsEnabled = true;
    public static bool startEnabled = true;
    public static bool updateEnabled = true;
    public static bool spawningEnabled = true;

    public static void InitializeTestingEnvironment(bool start, bool update, bool effects, bool asteroids, bool paused)
    {
        effectsEnabled = effects;
        startEnabled = start;
        updateEnabled = update;
        spawningEnabled = asteroids;
        IsPaused = paused;
    }

    /// <summary>
    /// Game logic
    /// </summary>
    public static GameManager instance;
    public static SpaceshipController spaceship;
    private static bool isPaused;
    public static int score;
    public static int scoreSinceLastDeath;

    public static bool IsPaused
    {
        get { return isPaused; }
        set {
            Time.timeScale = value ? 0.0f : 1.0f; 
            isPaused = value;
            }         //timescale is 0 when paused, 1 when unpaused
    }

    public static bool GameplayActive() => !(IsPaused || (SpaceshipIsActive() && spaceship.inBulletTime) || inCutscene);


    public static bool SpaceshipIsActive() { return spaceship != null; }

    public GameObject spaceshipPrefab;
    public GameObject asteroidPrefab;
    public GameObject bulletTimeAsteroidPrefab;
    public GameObject[] collectibles;
    public InGameMenuController inGameMenuController;

    public int asteroidCount;
    public float asteroidSpawnDelay = 0.5f;
    public float asteroidSpawnTimer = 0.0f;

    public const float SPACESHIP_RESPAWN_DELAY = 1.0f;

    public int deaths = 0;

    private ScoreCounter scoreCounter;
    
    //mobile specific controls
    public GameObject mobileInputCanvas;

    //public float respawnTimer = 1.0f;

    void OnEnable()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        
        Time.timeScale = 1.0f;
        IsPaused = false;
        score = 0;
        scoreSinceLastDeath = 0;
        inCutscene = false;
        nextCollectibleSpawnScore = COLLECTIBLE_SPAWN_SCORE_THRESHOLD;
        //scoreCounter = GameObject.Find("ScoreCounter").GetComponent<ScoreCounter>();
    }

    // Use this for initialization
    void Start () {
        if (startEnabled)
        {
            spaceship = Instantiate(spaceshipPrefab, Vector3.zero, Quaternion.identity).GetComponent<SpaceshipController>();
#if (UNITY_ANDROID || UNITY_IOS)
            Instantiate(mobileInputCanvas).transform.name = "MobileInputCanvas";
#endif
        }
    }

// Update is called once per frame
    void Update () {
        if (updateEnabled && GameplayActive())
        {
            UpdateTimers();
            if (asteroidSpawnTimer <= 0.0f && spawningEnabled)
                SpawnAsteroids();
        }

        if (SpaceshipIsActive()) //Testing
        {
            if (Input.GetKeyDown(KeyCode.N))
            {
                WipeAllAsteroids();
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha0))
            AddToScore(2500);
	}

    private void UpdateTimers()
    {
        if (asteroidSpawnTimer > 0.0f)
            asteroidSpawnTimer -= Time.deltaTime;
    }

    public void InitializeScene() //used in tests
    {
        if(spaceship == null)
            spaceship = Instantiate(spaceshipPrefab, Vector2.zero, Quaternion.identity).GetComponent<SpaceshipController>();
    }

    public void RespawnShip(float delay = SPACESHIP_RESPAWN_DELAY)
    {
        StartCoroutine(RespawnShipCoroutine(delay));
    }

    public IEnumerator RespawnShipCoroutine(float delay)
    {
        deaths++;
        if (LifeCounter.instance)
            LifeCounter.instance.SetLives(3 - deaths);
        yield return new WaitForSeconds(delay);
        scoreSinceLastDeath = 0;
        if (deaths < 3)
        {
            spaceship = Instantiate(spaceshipPrefab, Vector2.zero, Quaternion.identity).GetComponent<SpaceshipController>();
        }
        else
        {
            StartCoroutine(ReloadScene());
        }
    }

    public void SpawnAsteroids()
    {
        AsteroidController asteroid;  //for test purposes
        if (Camera.main != null)
        {
            Vector2 spawnPosition = Random.value > 0.5f ? Camera.main.ViewportToWorldPoint(new Vector2(Random.value > 0.5f ? Random.Range(-0.1f, -0.05f) : Random.Range(1.05f, 1.1f), Random.Range(-0.1f, 1.1f))) : Camera.main.ViewportToWorldPoint(new Vector2(Random.Range(-0.1f, 1.1f), Random.value > 0.5f ? Random.Range(-0.1f, -0.05f) : Random.Range(1.05f, 1.1f)));
            asteroid = Instantiate(asteroidPrefab, spawnPosition, Quaternion.identity).GetComponent<AsteroidController>();
            asteroid.SetDirection(Camera.main.ViewportToWorldPoint(new Vector2(Random.value, Random.value)));
        }
        else
            asteroid = Instantiate(asteroidPrefab, Vector2.zero, Quaternion.identity).GetComponent<AsteroidController>(); //for test purposes

        asteroid.SetSplitCount(Random.Range(0, 3));
        
        asteroidSpawnTimer = asteroidSpawnDelay;
        if (asteroidSpawnDelay > 0.3f)
            asteroidSpawnDelay -= 0.01f;
    }

    public const int COLLECTIBLE_SPAWN_SCORE_THRESHOLD = 15000;
    private static int nextCollectibleSpawnScore = COLLECTIBLE_SPAWN_SCORE_THRESHOLD; //will spawn a collectible item once the score threshold is reached
    private int collectibleSpawnIndex = 0;
    public void SpawnCollectibles()
    {
        if (Camera.main != null)
        {
            Vector2 spawnPosition = 
                Random.value > 0.5f ? 
                    Camera.main.ViewportToWorldPoint(
                        new Vector2(Random.value > 0.5f ? Random.Range(-0.1f, -0.05f) : Random.Range(1.05f, 1.1f), 
                        Random.Range(-0.1f, 1.1f))) : 
                    Camera.main.ViewportToWorldPoint(
                        new Vector2(Random.Range(-0.1f, 1.1f), 
                        Random.value > 0.5f ? Random.Range(-0.1f, -0.05f) : Random.Range(1.05f, 1.1f)));
            Instantiate(collectibles[collectibleSpawnIndex % collectibles.Length], spawnPosition, Quaternion.identity);
            collectibleSpawnIndex++;
        }
    }

    //TEMPORARY UNTIL PROPER UI IS IMPLEMENTED
    IEnumerator ReloadScene()
    {
        yield return new WaitForSeconds(3.0f);
        SceneManager.LoadScene("Scenes/04_Gameplay");
    }

    public static void AddAsteroidToScore(int splitCount)
    {
        switch (splitCount)
        {
            case 0:
                score += 1000;
                scoreSinceLastDeath += 1000;
                break;
            case 1:
                score += 500;
                scoreSinceLastDeath += 500;
                break;
            case 2:
                score += 250;
                scoreSinceLastDeath += 250;
                break;
        }

        CheckForScoreEvents();
    }

    public static void AddToScore(int amount)
    {
        score += amount;
        scoreSinceLastDeath += amount;
        CheckForScoreEvents();
    }

    public static void CheckForScoreEvents()
    {
        if (spawningEnabled && score >= nextCollectibleSpawnScore)
        {
            GameManager.instance.SpawnCollectibles();
            nextCollectibleSpawnScore += COLLECTIBLE_SPAWN_SCORE_THRESHOLD;
        }

        if (score >= BOSS_SPAWN_SCORE_THRESHOLD)
        {
            GameManager.instance.StartCoroutine(GameManager.instance.InitiateBossIntro());
        }

        if (SpaceshipIsActive())
            spaceship.UpdateWeapon(scoreSinceLastDeath);
        if (ScoreCounter.instance)
            ScoreCounter.instance.StartCounting(score);
    }

    List<BulletTimeAsteroidController> bulletTimeAsteroids;
    public void SpawnBulletTimeAsteroids()
    {
        bulletTimeAsteroids = new List<BulletTimeAsteroidController>();
        for (int i = 0; i < 20; i++)
        {
            BulletTimeAsteroidController asteroid;  //for test purposes
            if (Camera.main != null)
            {
                Vector3 spawnPosition = Camera.main.ViewportToWorldPoint(new Vector2(Random.Range(0.1f, 0.9f), Random.Range(0.1f, 0.9f))) + CameraController.instance.transform.forward * Random.Range (3f, 5f);
                asteroid = Instantiate(bulletTimeAsteroidPrefab, spawnPosition, Quaternion.identity).GetComponent<BulletTimeAsteroidController>();
                asteroid.transform.localScale = Vector3.one * 0.5f + Vector3.one * Random.value;
                bulletTimeAsteroids.Add(asteroid);
            }
        }
    }

    public void ClearBulletTimeAsteroids()
    {
        if (bulletTimeAsteroids != null)
        {
            foreach (BulletTimeAsteroidController asteroid in bulletTimeAsteroids)
            {
                Destroy(asteroid.gameObject);
            }
            bulletTimeAsteroids.Clear();
        }
    }

    public void WipeAllAsteroids()
    {
        AsteroidController[] asteroids = FindObjectsOfType<AsteroidController>();
        foreach(AsteroidController asteroid in asteroids)
        {
            asteroid.Split(true);
        }
        
        inGameMenuController?.TriggerWhiteout();
    }

    private static bool inCutscene = false;
    private const int BOSS_SPAWN_SCORE_THRESHOLD = 30000;
    private bool bossSequenceActivated = false;
    public IEnumerator InitiateBossIntro()
    {
        if (!bossSequenceActivated)
        {
            inCutscene = true;
            bossSequenceActivated = true;
            inGameMenuController.canvas.enabled = false;
            yield return SceneManager.LoadSceneAsync("Scenes/05_Boss_Intro", LoadSceneMode.Additive);

            //Disable multiple audio listeners warning
            CameraController.instance.listener.enabled = false;
        }
    }

    public GameObject starcruiserBoss;
    public void BeginBossBattle()
    {
        inCutscene = false;
        inGameMenuController.canvas.enabled = true;
        starcruiserBoss.SetActive(true);
        CameraController.instance.listener.enabled = true;
    }
}
