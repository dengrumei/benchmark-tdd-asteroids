﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySegment : DamageableObject
{
    private StarcruiserBoss starcruiser;
    public GameObject damageParticles;

    void Start()
    {
        starcruiser = transform.root.GetComponent<StarcruiserBoss>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Asteroids"))
        {
            if (Camera.main)
                Camera.main.GetComponent<CameraController>().ScreenShake(0.1f, 0.1f);

            collision.gameObject.GetComponent<AsteroidController>().Split(true);
        }
    }

    public override void TakeDamage(Vector3 hitPosition, Quaternion hitAngle)
    {
        GameObject particles = Instantiate(damageParticles, hitPosition, hitAngle);
        particles.transform.localScale = transform.localScale == Vector3.one ? transform.localScale : Vector3.one / 3.0f + transform.localScale / 2.0f;
        Destroy(particles, particles.GetComponent<ParticleSystem>().main.duration);
        starcruiser.CurrentLife--;
    }
}
