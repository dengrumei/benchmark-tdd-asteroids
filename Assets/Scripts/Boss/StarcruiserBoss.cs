﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarcruiserBoss : MonoBehaviour
{
    public AudioSource source;
    public EnemyCannon[] cannons;

    public const int MAXIMUM_LIFE = 100;
    public int CurrentLife 
    { 
        get => currentLife;
        set 
        {
            currentLife = value;
            if (currentLife < MAXIMUM_LIFE / 2.0f)
                foreach (EnemyCannon cannon in cannons)
                    cannon.SetWeapon(EnemyCannon.Weapon.Laser);
            if (currentLife <= 0)
                InitiateDestroySequence();
        }
    }
    
    private int currentLife = MAXIMUM_LIFE;
    private float stateTimer = 0.0f;

    public enum BossState
    {
        Intro,
        Idle,
        FlyOffscreen,
        Flyby
    }
    private BossState state;
    public BossState State 
    {   
        get => state;
        set 
        {
            randomInteger = Random.Range(0, flybyPositions.Length);
            stateTimer = 0.0f;
            state = value;
            switch (state)
            {
                case BossState.Intro:
                    transform.eulerAngles = new Vector3(0.0f, 0.0f, -180.0f);
                    transform.position = new Vector3(0.0f, 15.0f, 0.0f);
                    break;
                case BossState.Idle:
                    break;
                case BossState.FlyOffscreen:
                    source.Play();
                    break;
                case BossState.Flyby:
                    source.Play();
                    break;
            }
        }
    }

    Vector3[] flybyPositions =
    {
        new Vector3(15.0f, 0.0f, 0.0f),
        new Vector3(15.0f, 15.0f, 0.0f),
        new Vector3(0.0f, 15.0f, 0.0f),
        new Vector3(-15.0f, 15.0f, 0.0f),
        new Vector3(-15.0f, 0.0f, 0.0f),
        new Vector3(-15.0f, -15.0f, 0.0f),
        new Vector3(0.0f, -15.0f, 0.0f),
        new Vector3(15.0f, -15.0f, 0.0f)
    };

    int randomInteger;
    Vector3 defaultRotation;

    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        cannons = GetComponentsInChildren<EnemyCannon>();
        defaultRotation = transform.rotation.eulerAngles;
        State = BossState.Intro;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.GameplayActive())
        {
            stateTimer += Time.deltaTime;
            switch (State)
            {
                case BossState.Intro:
                    {
                        TurnTowardsSpaceship();

                        if (stateTimer <= 4.0f)
                        {
                            transform.position = Vector3.Lerp(transform.position, new Vector3(0.0f, 4.0f, 0.0f), Time.deltaTime);
                        }
                        else if (stateTimer <= 6.0f)
                        {

                        }
                        else
                            State = BossState.Idle;
                    }
                    break;
                case BossState.Idle:
                    {
                        if (stateTimer <= 10.0f)
                        {
                            TurnTowardsSpaceship();
                        }
                        else
                            State = BossState.FlyOffscreen;
                    }
                    break;
                case BossState.FlyOffscreen:
                    {
                        if (stateTimer <= 5.0f)
                        {
                            if (GameManager.SpaceshipIsActive())
                            {
                                float angle = Vector3.SignedAngle(GameManager.spaceship.transform.position - transform.position, Vector3.down, Vector3.back);
                                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(defaultRotation + new Vector3(0.0f, 0.0f, angle) + new Vector3(Mathf.Sin(Time.time * 1.27f), Mathf.Cos(Time.time * 0.84f), Mathf.Sin(Time.time * 1f)) * 15.0f), Time.deltaTime * 0.5f);
                                transform.position += Vector3.Lerp(transform.up, (GameManager.spaceship.transform.position - transform.position).normalized, Time.deltaTime) * Time.deltaTime * stateTimer * stateTimer;
                            }
                            else
                            {
                                transform.position += transform.up * Time.deltaTime * stateTimer * stateTimer;
                            }
                        }
                        else
                        {
                            State = BossState.Flyby;
                            transform.position = flybyPositions[randomInteger];
                            float angle = Vector3.SignedAngle(transform.position, Vector3.down, Vector3.back);
                            transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, angle));
                        }
                    }
                    break;
                case BossState.Flyby:
                    {
                        if (stateTimer <= 2.0f)
                        {

                        }
                        else if (stateTimer <= 10.0f)
                        {
                            transform.position += transform.up * Time.deltaTime * stateTimer;
                        }
                        else
                            State = BossState.Intro;
                    }
                    break;
            }
        }
    }

    private void TurnTowardsSpaceship()
    {
        if (GameManager.SpaceshipIsActive())
        {
            float angle = Vector3.SignedAngle(GameManager.spaceship.transform.position - transform.position, Vector3.down, Vector3.back);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(defaultRotation + new Vector3(0.0f, 0.0f, angle) + new Vector3(Mathf.Sin(Time.time * 1.27f), Mathf.Cos(Time.time * 0.84f), Mathf.Sin(Time.time * 1f)) * 15.0f), Time.deltaTime * 0.5f);
        }
        else
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(defaultRotation), Time.deltaTime * 0.5f);
        }
    }


    public void InitiateDestroySequence()
    {
        Destroy(gameObject);
        GameManager.instance.WipeAllAsteroids();
    }
}