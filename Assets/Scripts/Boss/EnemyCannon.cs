﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCannon : MonoBehaviour
{
    private StarcruiserBoss starcruiser;
    public GameObject chargingParticles;
    public WeaponList weaponList;

    float randomDuration;
    float stateTimer;

    public enum WeaponState
    {
        Targeting,
        Charging,
        Idle,
        Resetting
    }

    private WeaponState state;
    public WeaponState State 
    { 
        get => state;
        set {
            if (state == value)
                return;

            switch (value)
            {
                case WeaponState.Targeting:
                    break;
                case WeaponState.Charging:
                    GameObject go = Instantiate(chargingParticles, transform.GetChild(0));
                    Destroy(go, 1.5f);
                    break;
                case WeaponState.Idle:
                    break;
                case WeaponState.Resetting:
                    break;
            }

            randomDuration = Random.value;
            stateTimer = 0.0f;
            state = value;
        }
    }

    public enum Weapon
    {
        Basic,
        Laser
    }

    public Weapon currentWeapon = Weapon.Basic;
    private Vector3 defaultRotation;


    // Start is called before the first frame update
    void Start()
    {
        starcruiser = transform.root.GetComponent<StarcruiserBoss>();
        defaultRotation = transform.rotation.eulerAngles;
        state = WeaponState.Targeting;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.GameplayActive() && starcruiser.State == StarcruiserBoss.BossState.Idle)
        {
            stateTimer += Time.deltaTime;
            if (GameManager.SpaceshipIsActive())
            {
                switch (State)
                {
                    case WeaponState.Targeting:
                        {
                            float angle = Vector3.SignedAngle(GameManager.spaceship.transform.position - transform.position, Vector3.down, Vector3.back);
                            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(defaultRotation + new Vector3(0.0f, 0.0f, angle)), stateTimer / (0.5f + randomDuration));
                            if (stateTimer >= (0.5f + randomDuration))
                                State = WeaponState.Charging;
                        }
                        break;
                    case WeaponState.Charging:
                        if (stateTimer >= 1.5f)
                        {
                            Shoot();
                            State = WeaponState.Idle;
                        }
                        break;
                    case WeaponState.Idle:
                        {
                            switch (currentWeapon)
                            {
                                case Weapon.Basic:
                                    if (stateTimer >= 0.1f)
                                    {
                                        State = WeaponState.Resetting;
                                    }
                                    break;
                                case Weapon.Laser:
                                    if (stateTimer >= 1.0f)
                                    {
                                        State = WeaponState.Resetting;
                                    }
                                    else
                                    {
                                        float angle = Vector3.SignedAngle(GameManager.spaceship.transform.position - transform.position, Vector3.down, Vector3.back);
                                        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(defaultRotation + new Vector3(0.0f, 0.0f, angle)), Time.deltaTime);
                                    }
                                    break;
                            }
                        }
                        break;
                    case WeaponState.Resetting:
                        transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(Vector3.zero), stateTimer / 2.0f);
                        if (stateTimer >= 2.0f)
                            State = WeaponState.Targeting;

                        break;
                }
            }
            else
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(defaultRotation), Time.deltaTime);
            }
        }
    }




    public void Shoot()
    {
        switch (currentWeapon)
        {
            case Weapon.Basic:
                ProjectileController projectile = Instantiate(weaponList.weapons[(int)currentWeapon].weaponPrefab, transform.position, Quaternion.identity).GetComponent<ProjectileController>();
                projectile.SetDamageLayer("Spaceship");
                projectile.destroyOnCollision = false;
                projectile.SetDirection(transform.up);
                break;
            case Weapon.Laser:
                LaserController laser = Instantiate(weaponList.weapons[(int)currentWeapon].weaponPrefab, transform.position, Quaternion.identity).GetComponent<LaserController>();
                laser.SetDamageLayer("Spaceship");
                laser.transform.up = transform.up;
                laser.transform.parent = transform;
                break;
            default:
                Debug.LogError("Invalid weapon state.");
                break;
        }
    }


    public void SetWeapon(Weapon weapon)
    {
        currentWeapon = weapon;
    }
}