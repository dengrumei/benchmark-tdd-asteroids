﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCollectible : MonoBehaviour
{
    public Vector3 torque;
    public AudioSource source;

    private void Awake()
    {
        torque = Random.insideUnitSphere;
        torque = torque != Vector3.zero ? torque : Vector3.one; //avoiding test instability in case Random.insideUnitSphere hits Vector3.zero
        Rigidbody body = GetComponent<Rigidbody>();
        Vector2 force = ((Vector2)Camera.main.ViewportToWorldPoint(new Vector2(Random.Range(0.4f, 0.6f), Random.Range(0.4f, 0.6f))) - (Vector2)transform.position).normalized * 4.0f;
        body.AddTorque(torque);
        body.AddForce(force, ForceMode.Impulse);
    }

    private void Update()
    {
        if (GameManager.GameplayActive())
        {
            if (Vector2.Distance(transform.position, Vector2.zero) > 20.0f)
            Destroy(gameObject);
        }
    }

    public abstract void Collect();

    public void PlayCollectSound()
    {
        AudioSource audioSource = Instantiate(source);
        audioSource.Play();
        Destroy(audioSource.gameObject, 1.0f);
    }
}