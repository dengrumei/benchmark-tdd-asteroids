﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nuke : BaseCollectible
{
    public override void Collect()
    {
        if (GameManager.GameplayActive())
        {            
            if(GameManager.instance)
                GameManager.instance.WipeAllAsteroids();
                
            PlayCollectSound();
            Destroy(gameObject);
        }
    }
}
