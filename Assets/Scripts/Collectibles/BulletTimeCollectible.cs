﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTimeCollectible : BaseCollectible
{
    public override void Collect()
    {
        if (GameManager.GameplayActive())
        {            
            if(GameManager.instance)
                GameManager.spaceship.BulletTime(true);

            PlayCollectSound();
            Destroy(gameObject);
        }
    }
}
