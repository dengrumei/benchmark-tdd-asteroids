﻿using System.IO;
using UnityEditor;
using UnityEngine;
using System.Reflection;

[InitializeOnLoad]
 public class ImportStatus
 {
    public static string GetStatus()
    {
        var assembly = Assembly.GetAssembly(typeof(SceneView));
        var type = assembly.GetType("UnityEditor.LogEntries");
        var method = type.GetMethod("GetStatusText");
        string result = method.Invoke(null, new object[]{}) as string;
        return result;
    }

    public static void LogStatus()
    {
        string path = Directory.GetCurrentDirectory() + "/Info/";

        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

        string fullFilePath = path + "Status.json";
        if(!File.Exists(fullFilePath))
        {
            using (StreamWriter writer = new StreamWriter(File.Open(fullFilePath, FileMode.Append)))
            writer.Write( JsonUtility.ToJson(GetStatus()));
        }
    }

    static ImportStatus()
    {
        LogStatus();
    }
 }