﻿#pragma warning disable IDE1006 // Naming Styles
using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;
using UnityEngine.Rendering;
using NUnit.Framework;

public class CreateBuilds : MonoBehaviour
{
    public static Dictionary<string, GraphicsDeviceType[]> graphicsAPIDictionary = new Dictionary<string, GraphicsDeviceType[]>()
        {
            { "macOS", new GraphicsDeviceType[] {
                GraphicsDeviceType.Metal,
                GraphicsDeviceType.OpenGLCore } },
            { "Windows", new GraphicsDeviceType[] {
                GraphicsDeviceType.Direct3D11,
                GraphicsDeviceType.Direct3D12,
                GraphicsDeviceType.Vulkan,
                GraphicsDeviceType.OpenGLCore } },
            { "Android", new GraphicsDeviceType[] {
                GraphicsDeviceType.Vulkan,
                GraphicsDeviceType.OpenGLES3,
                GraphicsDeviceType.OpenGLES2 } },
            { "iOS", new GraphicsDeviceType[] {
                GraphicsDeviceType.Metal,
                GraphicsDeviceType.OpenGLES3,
                GraphicsDeviceType.OpenGLES2 } },
            { "Linux", new GraphicsDeviceType[] {
                GraphicsDeviceType.Vulkan,
                GraphicsDeviceType.OpenGLCore } },
            { "WebGL", new GraphicsDeviceType[]{}}
        };

    private const BuildOptions BOptions = BuildOptions.ShowBuiltPlayer;
        
    [Serializable]
    public class BuildResultFields
    {
        public string version;
        public string platform;
        public string totalSize;
        public string totalSizeMegaBytes;
        public string totalTime;
        public string totalErrors;
        public string totalWarnings;
    }
        
        
    private static string[] GetScenes()
    {
		List<string> sceneList = new List<string>();
        for(int i = 0; i < EditorBuildSettings.scenes.Length; i++)
        {
            Assert.NotNull(EditorBuildSettings.scenes[i], "Missing scene at index " + i);
			if (EditorBuildSettings.scenes[i].enabled)
				sceneList.Add(EditorBuildSettings.scenes[i].path);
		}
		return sceneList.ToArray();
    }

        
    private static void PrintReport(string name, BuildSummary summary)
    {
        //EditorPrefs.SetString("AndroidSdkRoot", Path.GetDirectoryName("C:\\Program Files\\OpenJDK"));
        string androidSdkLocation = EditorPrefs.GetString("AndroidSdkRoot");
        //string andriodNdkLocation = EditorPrefs.GetString("AndroidSdkRoot");
        string path = Directory.GetCurrentDirectory() + "/Info/" ;
            
        BuildResultFields newBuildInfo = new BuildResultFields();
            
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        
        using (StreamWriter writer = new StreamWriter(File.Open(path + "build-report-" + name + ".json", FileMode.Append)))
            
            switch (summary.result)
            {
                case BuildResult.Succeeded:
                    newBuildInfo.version = Application.unityVersion;
                    newBuildInfo.platform = summary.platform.ToString();
                    newBuildInfo.totalSize = summary.totalSize.ToString();
                    newBuildInfo.totalSizeMegaBytes = (Mathf.Round((float)summary.totalSize / 10000.0f) / 100).ToString();
                    newBuildInfo.totalTime = summary.totalTime.ToString();
                    newBuildInfo.totalErrors = summary.totalErrors.ToString();
                    newBuildInfo.totalWarnings = summary.totalWarnings.ToString();
                    
                        
                    writer.Write( JsonUtility.ToJson(newBuildInfo));
                        
                    Debug.Log("SDK location: " + androidSdkLocation);
                    //Debug.Log("SDK location: " + androidSdkLocation);
                    Debug.Log("Version: "  + Application.unityVersion);
                    Debug.Log("Build succeeded: " + summary.totalSize + " bytes." + " Platform: " + summary.platform);
                    Debug.Log("Build path: " + summary.outputPath);
                    Debug.Log("Total Time: " + summary.totalTime);
                    break;
                case BuildResult.Failed:
                    Debug.Log("Build failed");
                    break;
            }

            
    }

    public static BuildReport macOS(ScriptingImplementation scriptingBackend, ColorSpace colorSpace = ColorSpace.Gamma, GraphicsDeviceType[] targetAPI = null)
    {
        PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, scriptingBackend);
        PlayerSettings.colorSpace = colorSpace;
        SetGraphicsAPIs(BuildTarget.StandaloneOSX, targetAPI);


#if UNITY_STANDALONE_OSX && UNITY_2020_2_OR_NEWER
        string architecture = TestEnvironmnent.GetArguments("-architecture");
#if UNITY_2022_2_OR_NEWER
        if (Enum.TryParse(architecture, out UnityEditor.Build.OSArchitecture architectureChoice))
#else
        if (Enum.TryParse(architecture, out UnityEditor.OSXStandalone.MacOSArchitecture architectureChoice))
#endif
        {
            Debug.Log("Setting macOS build architecture to " + architectureChoice + ".");
            UnityEditor.OSXStandalone.UserBuildSettings.architecture = architectureChoice;
        }
        else
        {
            Debug.LogWarning("Defaulting to Intel 64-bit build architecture!");
#if UNITY_2022_2_OR_NEWER
            UnityEditor.OSXStandalone.UserBuildSettings.architecture = UnityEditor.Build.OSArchitecture.x64;
#else
            UnityEditor.OSXStandalone.UserBuildSettings.architecture = UnityEditor.OSXStandalone.MacOSArchitecture.x64;
#endif
        }
#endif

        string buildName = "Asteroids-" + PlayerSettings.GetScriptingBackend(BuildTargetGroup.Standalone).ToString() + "-" + (targetAPI == null ? "Auto" : PlayerSettings.GetGraphicsAPIs(BuildTarget.StandaloneOSX)[0].ToString()) + "-" + (PlayerSettings.colorSpace).ToString();

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            locationPathName = "Builds/MacOS/" + buildName,
            target = BuildTarget.StandaloneOSX,
            scenes = GetScenes(),
            options = BOptions,
        };

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        PrintReport(buildName, report.summary);
        return report;
    }
        
    public static BuildReport Windows(ScriptingImplementation scriptingBackend, ColorSpace colorSpace = ColorSpace.Gamma, GraphicsDeviceType[] targetAPI = null)
    {
        PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, scriptingBackend);
        PlayerSettings.colorSpace = colorSpace;
        SetGraphicsAPIs(BuildTarget.StandaloneWindows64, targetAPI);

        string buildName = "Asteroids-" + PlayerSettings.GetScriptingBackend(BuildTargetGroup.Standalone).ToString() + "-" + (targetAPI == null ? "Auto" : PlayerSettings.GetGraphicsAPIs(BuildTarget.StandaloneWindows64)[0].ToString()) + "-" + (PlayerSettings.colorSpace).ToString();

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            locationPathName = "Builds/Windows/" + buildName,
            target = BuildTarget.StandaloneWindows64,
            scenes = GetScenes(),
            options = BOptions,  
        };

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        PrintReport(buildName, report.summary);
        return report;
    }

    public static BuildReport Linux(ScriptingImplementation scriptingBackend, ColorSpace colorSpace = ColorSpace.Gamma, GraphicsDeviceType[] targetAPI = null)
    {
        PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, scriptingBackend);
        PlayerSettings.colorSpace = colorSpace;
        SetGraphicsAPIs(BuildTarget.StandaloneLinux64, targetAPI);

        string buildName = "Asteroids-" + PlayerSettings.GetScriptingBackend(BuildTargetGroup.Standalone).ToString() + "-" + (targetAPI == null ? "Auto" : PlayerSettings.GetGraphicsAPIs(BuildTarget.StandaloneLinux64)[0].ToString()) + "-" + (PlayerSettings.colorSpace).ToString();

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            locationPathName = "Builds/Linux/" + buildName,
            target = BuildTarget.StandaloneLinux64,
            scenes = GetScenes(),
            //options = BOptions,
        };

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        PrintReport(buildName, report.summary);
        return report;
    }

    public static BuildReport iOS(ScriptingImplementation scriptingBackend, ColorSpace colorSpace = ColorSpace.Gamma, GraphicsDeviceType[] targetAPI = null)
    {
        PlayerSettings.SetScriptingBackend(BuildTargetGroup.iOS, scriptingBackend);
        PlayerSettings.colorSpace = colorSpace;
        SetGraphicsAPIs(BuildTarget.iOS, targetAPI);

        string buildName = "Asteroids-" + PlayerSettings.GetScriptingBackend(BuildTargetGroup.iOS).ToString() + "-" + (targetAPI == null ? "Auto" : PlayerSettings.GetGraphicsAPIs(BuildTarget.iOS)[0].ToString()) + "-" + (PlayerSettings.colorSpace).ToString();

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            locationPathName = "Builds/iOS/" + buildName,
            target = BuildTarget.iOS,
            scenes = GetScenes(),
            options = BuildOptions.Development | BuildOptions.ShowBuiltPlayer,
        };

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        PrintReport(buildName, report.summary);
        return report;
    }

    public static BuildReport Android(ScriptingImplementation scriptingBackend, ColorSpace colorSpace = ColorSpace.Gamma, GraphicsDeviceType[] targetAPI = null)
    {
        PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, scriptingBackend);
        PlayerSettings.colorSpace = colorSpace;
        SetGraphicsAPIs(BuildTarget.Android, colorSpace == ColorSpace.Linear ? new GraphicsDeviceType[]{GraphicsDeviceType.OpenGLES3, GraphicsDeviceType.Vulkan} : targetAPI); //OpenGLES2 doesn't support linear color space

        string buildName = "Asteroids-" + PlayerSettings.GetScriptingBackend(BuildTargetGroup.Android).ToString() + "-" + (targetAPI == null ? "Auto" : PlayerSettings.GetGraphicsAPIs(BuildTarget.Android)[0].ToString()) + "-" + (PlayerSettings.colorSpace).ToString();

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            locationPathName = "Builds/Android/" + buildName,
            target = BuildTarget.Android,
            scenes = GetScenes(),
            options = BuildOptions.Development | BuildOptions.ShowBuiltPlayer,
        };

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        PrintReport(buildName, report.summary);
        return report;
    }

    public static BuildReport WebGL(ScriptingImplementation scriptingBackend, ColorSpace colorSpace = ColorSpace.Gamma, GraphicsDeviceType[] targetAPI = null)
    {
        PlayerSettings.SetScriptingBackend(BuildTargetGroup.WebGL, scriptingBackend);
        PlayerSettings.colorSpace = colorSpace;
        SetGraphicsAPIs(BuildTarget.WebGL, targetAPI);

        string buildName = "Asteroids-" + PlayerSettings.GetScriptingBackend(BuildTargetGroup.WebGL).ToString() + "-" + (targetAPI == null ? "Auto" : PlayerSettings.GetGraphicsAPIs(BuildTarget.WebGL)[0].ToString()) + "-" + (PlayerSettings.colorSpace).ToString();

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            locationPathName = "Builds/WebGL/" + buildName,
            target = BuildTarget.WebGL,
            scenes = GetScenes(),
            options = BuildOptions.Development | BuildOptions.ShowBuiltPlayer,
        };

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        PrintReport(buildName, report.summary);
        return report;
    }


    public static void SetGraphicsAPIs(BuildTarget platform, GraphicsDeviceType[] targetAPI = null)
    {
        if (targetAPI != null)
        {
            PlayerSettings.SetUseDefaultGraphicsAPIs(platform, false);
            PlayerSettings.SetGraphicsAPIs(platform, targetAPI);
        }
        else
        {
            PlayerSettings.SetUseDefaultGraphicsAPIs(platform, true);
        }
    }
}