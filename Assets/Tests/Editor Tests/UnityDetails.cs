﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEditorInternal;

 [InitializeOnLoad]
 public class UnityDetails
 {
    [Serializable]
    public class UnityVersionDetails
    {
        public string version;
        public string branch;
        public string fullVersion;
        public string versionDate;

        public UnityVersionDetails(string version, string branch, string fullVersion, string versionDate)
        {
            this.version = version;
            this.branch = branch;
            this.fullVersion = fullVersion;
            this.versionDate = versionDate;
        }
    }
    
    public static void LogUnityVersion()
    {
        string path = Directory.GetCurrentDirectory() + "/Info/";

        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

        UnityVersionDetails unityInfo = new UnityVersionDetails(
            Application.unityVersion, 
            InternalEditorUtility.GetUnityBuildBranch(),
            InternalEditorUtility.GetFullUnityVersion(),
            new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(InternalEditorUtility.GetUnityVersionDate()).ToString());

        string fullFilePath = path + unityInfo.fullVersion + ".json";
        if(!File.Exists(fullFilePath))
        {
            using (StreamWriter writer = new StreamWriter(File.Open(fullFilePath, FileMode.Append)))
            writer.Write( JsonUtility.ToJson(unityInfo));
        }
    }     
    
    static UnityDetails()
    {
        LogUnityVersion();
    }
 }