﻿#pragma warning disable IDE1006 // Naming Styles
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using NUnit.Framework;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.TestTools;

namespace Tests
{
    public class BuildTests
    {
        BuildReport result;

        public static BuildReport InvokeBuildMethod(string platform, ScriptingImplementation scriptingImplementation, ColorSpace colorSpace = ColorSpace.Gamma, GraphicsDeviceType[] targetAPI = null)
        {
            Type calledType = Type.GetType("CreateBuilds");
            BuildReport report = (BuildReport)calledType.InvokeMember(platform, BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Static, null, null, new object[] { scriptingImplementation, colorSpace, targetAPI });
            return report;
        }

        [Test]
        [Timeout(3600000)]
        public void BuildSucceeds_Graphics_API_Auto_Gamma()
        {
#if UNITY_2022_2
            if (SystemInfo.operatingSystem.Contains("Windows 7"))
            {
                LogAssert.Expect(LogType.Assert, new Regex("IsDenoiserLibraryAvailable"));
                LogAssert.Expect(LogType.Assert, new Regex("IsDenoiserLibraryAvailable"));
            }
#endif
            BuildWithGraphicsAPI(null, ColorSpace.Gamma);
        }

        [Test]
        [Timeout(3600000)]
        public void BuildSucceeds_Graphics_API_Auto_Linear()
        {
            BuildWithGraphicsAPI(null, ColorSpace.Linear);
        }

        [Test]
        [Timeout(3600000)]
        public void BuildSucceeds_Graphics_API_Direct3D11()
        {
            BuildWithGraphicsAPI(new GraphicsDeviceType[] { GraphicsDeviceType.Direct3D11 });
        }

        [Test]
        [Timeout(3600000)]
        public void BuildSucceeds_Graphics_API_Direct3D12()
        {
            BuildWithGraphicsAPI(new GraphicsDeviceType[] { GraphicsDeviceType.Direct3D12 });
        }

        [Test]
        [Timeout(3600000)]
        public void BuildSucceeds_Graphics_API_Vulkan()
        {
            BuildWithGraphicsAPI(new GraphicsDeviceType[] { GraphicsDeviceType.Vulkan });
        }

        [Test]
        [Timeout(3600000)]
        public void BuildSucceeds_Graphics_API_Metal()
        {
            BuildWithGraphicsAPI(new GraphicsDeviceType[] { GraphicsDeviceType.Metal });
        }

        [Test]
        [Timeout(3600000)]
        public void BuildSucceeds_Graphics_API_OpenGLCore()
        {
            BuildWithGraphicsAPI(new GraphicsDeviceType[] { GraphicsDeviceType.OpenGLCore });
        }

        [Test]
        [Timeout(3600000)]
        public void BuildSucceeds_Graphics_API_OpenGLES3()
        {
            BuildWithGraphicsAPI(new GraphicsDeviceType[] { GraphicsDeviceType.OpenGLES3 });
        }

        [Test]
        [Timeout(3600000)]
        public void BuildSucceeds_Graphics_API_OpenGLES2()
        {
            BuildWithGraphicsAPI(new GraphicsDeviceType[] { GraphicsDeviceType.OpenGLES2 });
        }

        public void BuildWithGraphicsAPI(GraphicsDeviceType[] targetAPI = null, ColorSpace colorSpace = ColorSpace.Gamma)
        {
            string platform = TestEnvironmnent.GetArguments("-buildPlatform");
            string backend = TestEnvironmnent.GetArguments("-scriptingBackend");
            //string platform = "macOS";
            //string backend = "Mono2x";

            Assert.NotNull(platform, "Missing platform command line argument.");
            Assert.NotNull(backend, "Missing backend command line argument.");

            ScriptingImplementation scriptingImplementation;
            Enum.TryParse(backend, out scriptingImplementation);

            Debug.Log("Attempting to build on " + platform + " with scripting backend " + backend + "...");

            GraphicsDeviceType[] graphicsAPIs;

            if (targetAPI != null)
            {
#if UNITY_STANDALONE_OSX && UNITY_2020_2_OR_NEWER
                string architecture = TestEnvironmnent.GetArguments("-architecture");
                if (targetAPI[0] == GraphicsDeviceType.OpenGLCore && architecture != null && architecture.Contains("ARM64"))
                {
                    Assert.Ignore();
                    return;
                }
#endif
                CreateBuilds.graphicsAPIDictionary.TryGetValue(platform, out graphicsAPIs);
                bool platformSupportsAPI = false;
                for (int i = 0; i < graphicsAPIs.Length; i++)
                {
                    if (targetAPI[0] == graphicsAPIs[i])
                        platformSupportsAPI = true;
                }


                //skip build test if the graphics api is not available
                if (!platformSupportsAPI)
                {
                    Assert.Ignore();
                    return;
                }
            }
            else if (platform == "WebGL" && colorSpace == ColorSpace.Linear)
            {
                Assert.Ignore();
                return;
            }

            Debug.Log("Building with Graphics API: " + (targetAPI == null ? "Auto" : targetAPI[0].ToString()) + "...\n");
            result = InvokeBuildMethod(platform, scriptingImplementation, colorSpace, targetAPI);
            Assert.AreEqual(BuildResult.Succeeded, result.summary.result); // check if the build succeeded by using BuildReport class
        }
    }
}
