﻿#pragma warning disable IDE1006 // Naming Styles
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class CollectibleTests : TestTemplate {
    //List of prefabs
    GameObject Prefab_Base_Collectible => GenericTestMethods.GetPrefab("Base_Collectible");
    GameObject Prefab_Nuke_Collectible => GenericTestMethods.GetPrefab("Nuke_Collectible");
    GameObject Prefab_Bullet_Time_Collectible => GenericTestMethods.GetPrefab("Bullet_Time_Collectible");
    GameObject Prefab_Spaceship => GenericTestMethods.GetPrefab("Spaceship");
    GameObject Prefab_Asteroid => GenericTestMethods.GetPrefab("Base_Asteroid");
    GameObject Prefab_MainCamera => GenericTestMethods.GetPrefab("Gameplay_Camera");
    GameObject Prefab_GameManager => GenericTestMethods.GetPrefab("GameManager");

    [SetUp]
    public override void Setup()
    {
        base.Setup();
        GameManager.InitializeTestingEnvironment(false, false, true, false, false);
        Object.Instantiate(Prefab_MainCamera);
    }

    [Test]
    public void _00PrefabsExist()
    {
        Assert.NotNull(Prefab_Nuke_Collectible, "Missing prefab.");
        Assert.NotNull(Prefab_Bullet_Time_Collectible, "Missing prefab.");
        Assert.NotNull(Prefab_Spaceship, "Missing prefab.");
        Assert.NotNull(Prefab_Asteroid, "Missing prefab.");
        Assert.NotNull(Prefab_MainCamera, "Missing prefab.");
    }

    [Test]
    public void _01ValidateCollectibleSetup()
    {
        GameObject baseCollectible = Prefab_Base_Collectible;
        GameObject nukeCollectible = Prefab_Nuke_Collectible;
        GameObject btCollectible = Prefab_Bullet_Time_Collectible;
        Assert.IsTrue(baseCollectible.tag == nukeCollectible.tag && nukeCollectible.tag == btCollectible.tag && btCollectible.tag == "Collectible", "Invalid tags. Expected 'Collectible', got " + baseCollectible.tag);
        Assert.IsTrue(baseCollectible.GetComponent<MeshFilter>() && nukeCollectible.GetComponent<MeshFilter>() && btCollectible.GetComponent<MeshFilter>(), "Missing mesh filter.");
        Assert.IsTrue(baseCollectible.GetComponent<MeshRenderer>() && nukeCollectible.GetComponent<MeshRenderer>() && btCollectible.GetComponent<MeshRenderer>(), "Missing mesh renderer.");
        Assert.IsTrue(baseCollectible.GetComponent<SphereCollider>() && nukeCollectible.GetComponent<SphereCollider>() && btCollectible.GetComponent<SphereCollider>(), "Missing collider.");
        Assert.IsTrue(baseCollectible.GetComponent<Rigidbody>() && nukeCollectible.GetComponent<Rigidbody>() && btCollectible.GetComponent<Rigidbody>(), "Missing rigidbody.");
        Assert.IsTrue(baseCollectible.GetComponent<Rigidbody>().useGravity == nukeCollectible.GetComponent<Rigidbody>().useGravity == btCollectible.GetComponent<Rigidbody>().useGravity == false, "Use gravity is enabled.");
        Assert.IsTrue(baseCollectible.GetComponent<Rigidbody>().isKinematic == nukeCollectible.GetComponent<Rigidbody>().isKinematic == btCollectible.GetComponent<Rigidbody>().isKinematic == false, "Is Kinematic is enabled.");
        Assert.IsTrue(baseCollectible.GetComponent<Rigidbody>().interpolation == nukeCollectible.GetComponent<Rigidbody>().interpolation && nukeCollectible.GetComponent<Rigidbody>().interpolation == btCollectible.GetComponent<Rigidbody>().interpolation && btCollectible.GetComponent<Rigidbody>().interpolation == RigidbodyInterpolation.None, "Interpolation isn't 'None'.");
        Assert.IsTrue(baseCollectible.GetComponent<Rigidbody>().collisionDetectionMode == nukeCollectible.GetComponent<Rigidbody>().collisionDetectionMode && nukeCollectible.GetComponent<Rigidbody>().collisionDetectionMode == btCollectible.GetComponent<Rigidbody>().collisionDetectionMode && btCollectible.GetComponent<Rigidbody>().collisionDetectionMode == CollisionDetectionMode.Continuous, "CollisionDetectionMode isn't 'Continuous'.");
        //checking prefab variant overrides
        Assert.IsTrue(baseCollectible.GetComponent<MeshRenderer>().sharedMaterial != nukeCollectible.GetComponent<MeshRenderer>().sharedMaterial && baseCollectible.GetComponent<MeshRenderer>().sharedMaterial != btCollectible.GetComponent<MeshRenderer>().sharedMaterial && btCollectible.GetComponent<MeshRenderer>().sharedMaterial   != nukeCollectible.GetComponent<MeshRenderer>().sharedMaterial, "Prefab variant mesh renderer material overrides are messed up yo.");
        Assert.IsTrue(!baseCollectible.GetComponent<Nuke>() && nukeCollectible.GetComponent<Nuke>() && !btCollectible.GetComponent<Nuke>());
        Assert.IsTrue(!baseCollectible.GetComponent<BulletTimeCollectible>() && !nukeCollectible.GetComponent<BulletTimeCollectible>() && btCollectible.GetComponent<BulletTimeCollectible>());
        Assert.IsTrue(nukeCollectible.GetComponent<BaseCollectible>().source != null && btCollectible.GetComponent<BaseCollectible>().source != null, "Missing audioSource reference.");
    }

    [UnityTest]
    public IEnumerator _02CollectibleHasAngularVelocity()
    {
        for (int j = 0; j < 10; j++)
        {
            BaseCollectible collectible = Object.Instantiate(Prefab_Nuke_Collectible, Vector3.zero, Quaternion.identity).GetComponent<BaseCollectible>();
            Rigidbody rigidbody = collectible.GetComponent<Rigidbody>();
            Vector3 initialTransformEulerRotation = collectible.transform.rotation.eulerAngles;
            Vector3 initialRigidbodyEulerRotation = rigidbody.rotation.eulerAngles;
            default(Assert).VectorsAreEqual(initialTransformEulerRotation, initialRigidbodyEulerRotation, 0.001f, "Rigidbody rotation (" + initialRigidbodyEulerRotation + ") != transform.rotation (" + initialTransformEulerRotation + ").");
            Assert.NotNull(rigidbody);
            yield return new WaitForFixedUpdate();
            yield return null;
            Vector3 secondTransformEulerRotation = collectible.transform.rotation.eulerAngles;
            Vector3 secondRigidbodyEulerRotation = rigidbody.rotation.eulerAngles;
            default(Assert).VectorsAreEqual(secondTransformEulerRotation, secondRigidbodyEulerRotation, 0.001f, "Rigidbody rotation (" + secondRigidbodyEulerRotation + ") != transform.rotation (" + secondTransformEulerRotation + ").");
            Assert.IsTrue(rigidbody.angularVelocity != Vector3.zero, "Rigidbody.angularVelocity is zero.");
            Assert.IsTrue(Vector3.Dot(rigidbody.angularVelocity.normalized, collectible.torque.normalized) > 0.9f, "Initial torque and angular velocity mismatch (" + Vector2.Dot(rigidbody.angularVelocity.normalized, collectible.torque.normalized) + "), should be close to identical.");
            Teardown();
            Setup();
        }
    }

    [UnityTest]
    public IEnumerator _03CollectiblesAreDestroyedOnCollect()
    {
        Object.Instantiate(Prefab_Spaceship);
        GameObject collectible1 = Object.Instantiate(Prefab_Nuke_Collectible, Vector3.zero, Quaternion.identity);
        yield return new WaitForFixedUpdate();
        yield return null;
        Assert.IsTrue(collectible1 == null);
        GameObject collectible2 = Object.Instantiate(Prefab_Bullet_Time_Collectible, Vector3.zero, Quaternion.identity);
        yield return new WaitForFixedUpdate();
        yield return null;
        Assert.IsTrue(collectible2 == null);
    }

    [UnityTest]
    public IEnumerator _04CollectiblesPlayAudioOnCollect()
    {
        Object.Instantiate(Prefab_Spaceship);
        Object.Instantiate(Prefab_Nuke_Collectible, Vector3.zero, Quaternion.identity);
        yield return new WaitForFixedUpdate();
        yield return null;
        AudioSource audioSource = Object.FindObjectOfType<AudioSource>();
        Assert.NotNull(audioSource, "Audio object was not spawned.");
        Assert.NotNull(audioSource.clip, "Audio object has no audio clip.");
        Assert.IsTrue(audioSource.clip.name == "sci-fi_power_up_10", "Invalid audio clip name.");
        Assert.IsTrue(audioSource.isPlaying, "Audio clip not playing.");
        Object.DestroyImmediate(audioSource.gameObject);

        yield return null;

        Object.Instantiate(Prefab_Bullet_Time_Collectible, Vector3.zero, Quaternion.identity);
        yield return new WaitForFixedUpdate();
        yield return null;
        audioSource = Object.FindObjectOfType<AudioSource>();
        Assert.NotNull(audioSource, "Audio object was not spawned.");
        Assert.NotNull(audioSource.clip, "Audio object has no audio clip.");
        Assert.IsTrue(audioSource.clip.name == "sci-fi_power_up_10", "Invalid audio clip name.");
        Assert.IsTrue(audioSource.isPlaying, "Audio clip not playing.");
    }

    [UnityTest]
    public IEnumerator _05CollectiblesMoveTowardsViewport()
    {
        for (int j = 0; j < 10; j++)
        {
            Object.Instantiate(Prefab_MainCamera);
            BaseCollectible collectible = Object.Instantiate(Prefab_Nuke_Collectible, new Vector2(Mathf.Sin(0.2f * (j % 5) * Mathf.PI * 2), Mathf.Cos(0.2f * (j % 5) * Mathf.PI * 2)).normalized * 10, Quaternion.identity).GetComponent<BaseCollectible>();
            yield return null;
            Assert.NotNull(collectible, "Could not find the Collectible object.");
            Rigidbody rigidbody = collectible.GetComponent<Rigidbody>();
            Assert.NotNull(rigidbody);
            yield return new WaitForFixedUpdate();
            yield return null;

            Vector2 initialVelocity = rigidbody.velocity;
            Assert.IsTrue((initialVelocity).magnitude > 0.0f, "Rigidbody.velocity is zero.)");
            Vector2 initialTransformPosition = collectible.transform.position;
            Vector2 initialRigidbodyPosition = rigidbody.position;
            default(Assert).VectorsAreEqual(initialTransformPosition, initialRigidbodyPosition, 0.001f, "Rigidbody position (" + initialRigidbodyPosition + ") != transform.position (" + initialTransformPosition + ").");
            yield return new WaitForFixedUpdate();
            yield return null;

            Vector2 secondTransformPosition = collectible.transform.position;
            Vector2 secondRigidbodyPosition = rigidbody.position;
            default(Assert).VectorsAreEqual(secondTransformPosition, secondRigidbodyPosition, 0.001f, "Rigidbody position (" + secondRigidbodyPosition + ") != transform.position (" + secondTransformPosition + ").");
            Vector2 transformMovementDirection = (secondTransformPosition - initialTransformPosition).normalized;
            Vector2 rigidbodyMovementDirection = (secondRigidbodyPosition - initialRigidbodyPosition).normalized;

            Assert.IsTrue((secondTransformPosition - initialTransformPosition).magnitude > 0.0f, "Collectible transform is not moving.)");
            Assert.IsTrue((secondRigidbodyPosition - initialRigidbodyPosition).magnitude > 0.0f, "Collectible rigidbody is not moving.)");

            float transformDotProduct = Vector2.Dot((Vector2.zero - initialTransformPosition).normalized, transformMovementDirection);
            float rigidbodyDotProduct = Vector2.Dot((Vector2.zero - initialRigidbodyPosition).normalized, rigidbodyMovementDirection);
            float velocityDotProduct = Vector2.Dot((Vector2.zero - initialRigidbodyPosition).normalized, initialVelocity.normalized);

            Assert.IsTrue(transformDotProduct > 0.0f, "Collectible transform is moving in the wrong direction. Dot product: (" + transformDotProduct + ")");
            Assert.IsTrue(rigidbodyDotProduct > 0.0f, "Collectible rigidbody is moving in the wrong direction. Dot product: (" + rigidbodyDotProduct + ")");
            Assert.IsTrue(velocityDotProduct > 0.0f, "Collectible velocity is pointing in the wrong direction. Dot product: (" + velocityDotProduct + ")");

            Assert.IsTrue(Vector2.Dot(transformMovementDirection, rigidbodyMovementDirection) > 0.9f, "Transform movement direction and Rigidbody movement direction vector mismatch. Dot product: (" + Vector2.Dot(transformMovementDirection, rigidbodyMovementDirection) + ", expected at least >0.9f");
            Assert.IsTrue(Vector2.Dot(transformMovementDirection, initialVelocity.normalized) > 0.9f, "Transform movement direction and Rigidbody velocity vector mismatch. Dot product: (" + Vector2.Dot(transformMovementDirection, initialVelocity.normalized) + "), expected at least >0.9f");
            Assert.IsTrue(Vector2.Dot(rigidbodyMovementDirection, initialVelocity.normalized) > 0.9f, "Rigidbody movement direction and Rigidbody velocity vector mismatch. Dot product: (" + Vector2.Dot(rigidbodyMovementDirection, initialVelocity.normalized) + "), expected at least >0.9f");
            Teardown();
            Setup();
        }
    }
}