﻿#pragma warning disable IDE1006 // Naming Styles
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Linq;
using TMPro;

public class Opening_Intro_Tests : TestTemplate{
    //List of prefabs
    GameObject Prefab_Intro_Canvas => GenericTestMethods.GetPrefab("Opening_Intro_Canvas");
    GameObject Prefab_Intro_Camera => GenericTestMethods.GetPrefab("Opening_Intro_Camera");
    GameObject Prefab_Intro_Music => GenericTestMethods.GetPrefab("Opening_Intro_Music");
    GameObject Prefab_Intro_Skipper => GenericTestMethods.GetPrefab("Intro_Skipper");

    /*//experimenting using addressables package to load prefabs for tests
    IList<GameObject> addressables;

    //Fetch prefab by name
    public GameObject GetPrefab(string name)
    {
        Assert.NotNull(addressables, "Missing addressables. List is null.");
        Assert.IsTrue(addressables.Count > 0, "Missing addressables. List is empty.");
        GameObject gameObject = addressables.Cast<Object>().SingleOrDefault(i => i.name == name) as GameObject;
        Assert.NotNull(gameObject, "Failed to find " + name + " prefab.");
        return gameObject;
    }

    //[SetUp] //2020-04-02 Addressables don't have synchronous load at the moment :(
    //Get all prefabs with Addressable Label "IntroTests"
    public IEnumerator LoadAssets()
    {
        AsyncOperationHandle<IList<GameObject>> handle = Addressables.LoadAssetsAsync<GameObject>(this.GetType().Name, null);
        yield return handle;
        if (handle.Status == AsyncOperationStatus.Succeeded)
        {
            addressables = handle.Result;
            Addressables.Release(handle);
        }
        else
        {
            Assert.Fail("Missing addressables.");
        }
        yield return null;
    }
    */

    [Test]
    public void _00PrefabsExist()
    {
        Assert.NotNull(Prefab_Intro_Canvas,     "Missing prefab.");
        Assert.NotNull(Prefab_Intro_Camera,     "Missing prefab.");
        Assert.NotNull(Prefab_Intro_Skipper,    "Missing prefab.");
        Assert.NotNull(Prefab_Intro_Music,      "Missing prefab.");
    }

    [UnityTest]
    public IEnumerator _01SceneExists()
    {
        SceneManager.LoadScene(TestConstants.OPENING_INTRO_SCENE_NAME);
        yield return null;
        Assert.IsTrue(SceneManager.GetActiveScene().name == TestConstants.OPENING_INTRO_SCENE_NAME.Split('/')[1], "Could not find " + TestConstants.OPENING_INTRO_SCENE_NAME + " scene.");
    }

    [Test]
    public void _02ValidateCanvasComponentSettings()
    {
        Assert.IsTrue(Prefab_Intro_Canvas.GetComponent<Canvas>().renderMode == RenderMode.WorldSpace, "Invalid Canvas RenderMode setting, should be RenderMode.WorldSpace");
    }

    [UnityTest]
    public IEnumerator _03CameraExistsInScene()
    {
        SceneManager.LoadScene(TestConstants.OPENING_INTRO_SCENE_NAME);
        yield return null;
        Assert.IsTrue(Object.FindObjectOfType<Camera>(), "Could not find IntroCamera in the Intro scene.");
    }

    [Test]
    public void _04ValidateCameraSettings()
    {
        Assert.IsTrue(Prefab_Intro_Camera.GetComponent<Camera>().clearFlags == CameraClearFlags.SolidColor, "Invalid Camera Clear Flags setting, should be CameraClearFlags.SolidColor");
        Assert.IsTrue(Prefab_Intro_Camera.GetComponent<Camera>().backgroundColor.Equals(Color.black), "Invalid Camera Background color setting, should be Black");
        Assert.IsTrue(Prefab_Intro_Camera.GetComponent<Camera>().orthographic == false, "Invalid Camera Projection setting, should be Perspective");
        Assert.NotNull(Prefab_Intro_Camera.GetComponent<AudioListener>(), "Missing Audio Listener component on the IntroCamera prefab.");
    }

    [UnityTest]
    public IEnumerator _05IntroSceneContainsRequiredPrefabs()
    {
        SceneManager.LoadScene(TestConstants.OPENING_INTRO_SCENE_NAME);
        yield return null;
        Assert.NotNull(GameObject.Find(Prefab_Intro_Canvas.name),   "Missing prefab in scene.");
        Assert.NotNull(GameObject.Find(Prefab_Intro_Camera.name),   "Missing prefab in scene.");
        Assert.NotNull(GameObject.Find(Prefab_Intro_Skipper.name),  "Missing prefab in scene.");
        Assert.NotNull(GameObject.Find(Prefab_Intro_Music.name),    "Missing prefab in scene.");
        //TODO check by prefab reference while in Editor, not by name
    }

    [UnityTest]
    public IEnumerator _06IntroSceneSpawnsStarParticles()
    {
        SceneManager.LoadScene(TestConstants.OPENING_INTRO_SCENE_NAME);
        yield return null;
        GameObject camera = GameObject.Find("Opening_Intro_Camera");
        Assert.NotNull(camera, "Missing Camera.");
        Assert.NotNull(camera.transform.GetChild(0) || camera.transform.GetChild(0).name != "Stars" || camera.transform.GetChild(0).GetComponent<ParticleSystem>() == null, "Missing Stars ParticleSystem.");
        ParticleSystem particleSystem = camera.transform.GetChild(0).GetComponent<ParticleSystem>();
        Assert.IsTrue(particleSystem.isPlaying && particleSystem.particleCount > 0, "ParticleSystem failed to spawn particles/prewarm");
    }

    [UnityTest]
    public IEnumerator _07IntroSkipperSuccessfullyLoadsNextScene()
    {
        SceneManager.LoadScene(TestConstants.OPENING_INTRO_SCENE_NAME);
        yield return null;
        Scene initialScene = SceneManager.GetActiveScene();
        int initialBuildIndex = initialScene.buildIndex;
        GameObject skipper = GameObject.Find("Intro_Skipper");
        Assert.NotNull(skipper, "Missing IntroSkipper.");
        IntroSkipper.Continue();
        yield return null;
        Assert.IsTrue(SceneManager.GetActiveScene() != initialScene, "IntroSkipper failed to load the next scene.");
        Assert.IsTrue(SceneManager.GetActiveScene().buildIndex == initialBuildIndex + 1, "IntroSkipper didn't load the correct scene.");
    }

    [UnityTest]
    public IEnumerator _08ValidateIntroTextMeshProSetup()
    {
        SceneManager.LoadScene(TestConstants.OPENING_INTRO_SCENE_NAME);
        yield return null;
        GameObject canvas = GameObject.Find("Opening_Intro_Canvas");
        Assert.NotNull(canvas.transform.GetChild(0), "Missing IntroCanvas children.");
        TextMeshProUGUI introText = canvas.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        Assert.IsTrue(introText.alignment == TextAlignmentOptions.TopFlush && introText.alignment.HasFlag(TextAlignmentOptions.TopFlush), "Invalid text alignment.");
        Assert.IsTrue(introText.fontStyle == FontStyles.Bold && introText.fontStyle.HasFlag(FontStyles.Bold), "Text is not bold.");
        Assert.NotNull(introText.font, "Missing font asset.");
        Assert.IsTrue(introText.font.name.Contains("LiberationSans"), "Wrong font asset.");
        Assert.IsTrue(introText.enableWordWrapping == true, "Wrapping is disabled.");
        Assert.IsTrue(introText.overflowMode == TextOverflowModes.Overflow, "Invalid Overflow mode.");
        Assert.IsTrue(introText.richText, "Rich text is disabled");
        Assert.IsTrue(introText.fontSize.Equals(36f), "Wrong font size.");
        Assert.NotNull(introText, "Missing TextMeshProUGUI component on IntroText.");
        yield return null;
        //Checking for text wrapping bugs https://fogbugz.unity3d.com/f/cases/1235695/
        Assert.IsTrue(introText.textInfo.lineCount == 19, "Intro text is longer than expected. Expected linecount is 19, but was " + introText.textInfo.lineCount + ". Reported at https://fogbugz.unity3d.com/f/cases/1235695/");
    }

    [UnityTest]
    public IEnumerator _09ValidateIntroTextAnimatorSetup()
    {
        SceneManager.LoadScene(TestConstants.OPENING_INTRO_SCENE_NAME);
        yield return null;
        GameObject canvas = GameObject.Find("Opening_Intro_Canvas");
        Assert.NotNull(canvas.transform.GetChild(0), "Missing IntroCanvas children.");
        Animator introText = canvas.transform.GetChild(0).GetComponent<Animator>();
        Assert.NotNull(introText, "Missing Animator component on IntroText.");
        Assert.NotNull(introText.runtimeAnimatorController, "Missing Animator Controller.");
        Assert.IsTrue(introText.runtimeAnimatorController.name == "IntroText", "Missing Animator Controller.");
        Assert.IsTrue(introText.cullingMode == AnimatorCullingMode.AlwaysAnimate, "Invalid Culling Mode.");
        Assert.NotNull(introText.GetComponent<SceneLoader>(), "Missing SceneLoader script. Scene loading event will fail.");
    }


    [UnityTest]
    public IEnumerator _10IntroTextAnimationMovesTextAnchoredPosition()
    {
        SceneManager.LoadScene(TestConstants.OPENING_INTRO_SCENE_NAME);
        yield return null;
        Animator introText = GameObject.Find("Opening_Intro_Canvas").transform.GetChild(0).GetComponent<Animator>();
        RectTransform introTextRect = introText.GetComponent<RectTransform>();
        Vector3 initialPosition = introTextRect.anchoredPosition;
        introText.Play("IntroCrawl", 0, 0.33f); 
        yield return null;
        Vector3 secondPosition = introTextRect.anchoredPosition;
        introText.Play("IntroCrawl", 0, 0.66f); 
        yield return null;
        Vector3 thirdPosition = introTextRect.anchoredPosition;
        Assert.IsTrue(initialPosition.y < secondPosition.y, "IntroText anchored position remains unchanged.");
        Assert.IsTrue(secondPosition.y < thirdPosition.y, "IntroText anchored position remains unchanged.");
    }

    [UnityTest]
    public IEnumerator _11IntroTextAnimationLoadsNextScene()
    {
        SceneManager.LoadScene(TestConstants.OPENING_INTRO_SCENE_NAME);
        yield return null;
        Scene initialScene = SceneManager.GetActiveScene();
        Animator introText = GameObject.Find("Opening_Intro_Canvas").transform.GetChild(0).GetComponent<Animator>();
        introText.Play("IntroCrawl", 0, 0.99f);

        float timeout = 10.0f;
        while (timeout > 0.0f && SceneManager.GetActiveScene() == initialScene)
        {
            timeout -= Time.deltaTime;
            yield return null;
        }

        Assert.IsTrue(SceneManager.GetActiveScene() != initialScene, "IntroText animation failed to load the next scene.");
        Assert.IsTrue(SceneManager.GetActiveScene().name == TestConstants.MENU_SCENE_NAME.Split('/')[1], "Failed to load Menu scene.");
    }

    [UnityTest]
    public IEnumerator _12IntroMusicPlaysOnSceneLoad()
    {
        SceneManager.LoadScene(TestConstants.OPENING_INTRO_SCENE_NAME);
        yield return null;
        AudioSource introMusic = GameObject.Find("Opening_Intro_Music").GetComponent<AudioSource>();
        Assert.IsTrue(introMusic.playOnAwake == true, "IntroMusic Play On Awake is disabled.");
        Assert.IsTrue(introMusic.loop == false, "IntroMusic loop is enabled.");
        Assert.NotNull(introMusic.clip, "Missing Audio Clip.");
        Assert.IsTrue(introMusic.clip.name == "music_cinematic_reveal", "Wrong Audio Clip.");
        AudioListener.volume = 1.0f;
        float initialTime = introMusic.time;
        yield return new WaitForSeconds(0.1f);
        Assert.IsTrue(initialTime < introMusic.time, "Music playback time is unchanged.");
        AudioListener.volume = 0.0f;
    }
}
