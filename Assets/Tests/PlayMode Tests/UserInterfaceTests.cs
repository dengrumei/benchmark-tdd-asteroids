﻿#pragma warning disable IDE1006 // Naming Styles
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class UserInterfaceTests : TestTemplate
{
    //List of prefabs
    GameObject Prefab_GameManager => GenericTestMethods.GetPrefab("GameManager");
    GameObject Prefab_MobileInput => GenericTestMethods.GetPrefab("Mobile_Input");
    GameObject Prefab_InGameMenu => GenericTestMethods.GetPrefab("InGame_Menu");

    [SetUp]
    public override void Setup()
    {
        base.Setup();
        GameManager.InitializeTestingEnvironment(true, false, false, false, false);
    }

    [UnityTest]
    public IEnumerator _00EventSystemExistsInMainScene()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_SCENE_NAME);
        yield return null;
        Assert.NotNull(Object.FindObjectOfType<EventSystem>());
    }

    [Test]
    public void _01MobileInputPrefabExists()
    {
        Assert.NotNull(Prefab_MobileInput);
        Assert.IsTrue(Prefab_MobileInput.GetComponent<Canvas>().renderMode == RenderMode.ScreenSpaceOverlay);
        Assert.IsTrue(Prefab_MobileInput.GetComponent<CanvasScaler>().uiScaleMode == CanvasScaler.ScaleMode.ScaleWithScreenSize);
        Assert.IsTrue(Prefab_MobileInput.GetComponent<CanvasScaler>().screenMatchMode == CanvasScaler.ScreenMatchMode.Expand);
    }

    [Test]
    public void _02MobileInputAnalogExists()
    {
        Transform analogArea = Prefab_MobileInput.transform.GetChild(0);
        Transform analog = analogArea.GetChild(0);
        Assert.NotNull(analogArea);
        Assert.IsTrue(analogArea.name == "AnalogArea");   //making sure if it's the correct transform.GetChild
        Assert.NotNull(analogArea.GetComponent<Image>());
        Assert.NotNull(analogArea.GetComponent<Image>().sprite);

        Assert.NotNull(analog);
        Assert.IsTrue(analog.name == "LeftAnalog");       //making sure if it's the correct transform.GetChild
        Assert.NotNull(analog.GetComponent<MobileInputAnalog>());
        Assert.NotNull(analog.GetComponent<Button>());
        Assert.NotNull(analog.GetComponent<Image>());
        Assert.NotNull(analog.GetComponent<Image>().sprite);
    }

    [Test]
    public void _02MobileInputFireButtonExists()
    {
        Transform fireButton = Prefab_MobileInput.transform.GetChild(1);
        Assert.NotNull(fireButton != null);
        Assert.IsTrue(fireButton.name == "FireButton");             //making sure if it's the correct transform.GetChild
        Assert.NotNull(fireButton.GetComponent<MobileInputButton>());
        Assert.NotNull(fireButton.GetComponent<Button>());
        Assert.NotNull(fireButton.GetComponent<Image>());
        Assert.NotNull(fireButton.GetComponent<Image>().sprite);
    }

#if !(UNITY_ANDROID || UNITY_IPHONE)
    [UnityTest]
    public IEnumerator _03MobileControlsAreNotInstantiatedOnStandalone()
    {
        Object.Instantiate(Prefab_GameManager);
        GameManager.InitializeTestingEnvironment(true, false, false, false, false);

        yield return null;
        Assert.IsNull(GameObject.Find("MobileInputCanvas"));
    }
#else
    [UnityTest]
    public IEnumerator _03MobileInputAreInstantiatedOnMobile()
    {   
        Object.Instantiate(Prefab_GameManager);
        GameManager.InitializeTestingEnvironment(true, false, false, false, false);
        yield return null;
        Assert.NotNull(GameObject.Find("MobileInputCanvas"));
    }
#endif

    [UnityTest]
    public IEnumerator _04MobileInputFireButtonFiresProjectiles()
    {
        Object.Instantiate(Prefab_GameManager).GetComponent<GameManager>();
        GameManager.InitializeTestingEnvironment(true, false, false, false, false);

        yield return null;
        Object.Instantiate(Prefab_MobileInput).transform.GetChild(1).GetComponent<Button>().onClick.Invoke();
        Assert.NotNull(Object.FindObjectsOfType<ProjectileController>());
    }

    [UnityTest]
    public IEnumerator _05MobileInputAnalogAffectsSpaceshipMovement()
    {
        Object.Instantiate(Prefab_GameManager).GetComponent<GameManager>();
        GameManager.InitializeTestingEnvironment(true, false, false, false, false);

        yield return null;
        Object.Instantiate(Prefab_MobileInput).transform.GetChild(0).GetChild(0).GetComponent<MobileInputAnalog>().output = Vector2.up;
        yield return null;
        yield return null;
        Assert.IsTrue(GameManager.spaceship.transform.position.y > 0.0f && GameManager.spaceship.transform.position.x == 0.0f);
    }

    [UnityTest]
    public IEnumerator _06MainMenuSceneExists()
    {
        SceneManager.LoadScene(TestConstants.MENU_SCENE_NAME);
        yield return null;
        Assert.IsTrue(SceneManager.GetActiveScene().name == TestConstants.MENU_SCENE_NAME.Split('/')[1]);
    }


    [UnityTest]
    public IEnumerator _07MainMenuSceneContainsUIElements()
    {
        SceneManager.LoadScene(TestConstants.MENU_SCENE_NAME);
        yield return null;
        RectTransform[] rects = Object.FindObjectsOfType<RectTransform>();                  //scene has recttransforms
        Assert.IsTrue(rects.Length > 0);
        Canvas canvas = Object.FindObjectOfType<Canvas>();
        Assert.NotNull(canvas);                                                             //scene has a canvas
        Assert.NotNull(Object.FindObjectOfType<MainMenuController>());
        Assert.IsTrue(canvas.name == "MainMenu");
        Assert.NotNull(Object.FindObjectOfType<EventSystem>());                             //scene has an event system
        Assert.NotNull(canvas.transform.GetChild(0).GetComponent<VerticalLayoutGroup>());  //canvas has a button container
        Assert.IsTrue(canvas.transform.GetChild(0).childCount > 0);                     //button container has children
    }

    [UnityTest]
    public IEnumerator _08MainMenuSceneHasGameTitleLogo()
    {
        SceneManager.LoadScene(TestConstants.MENU_SCENE_NAME);
        yield return null;
        Assert.NotNull(GameObject.Find("Title"));
        Image image = GameObject.Find("Title").GetComponent<Image>();
        Assert.NotNull(image);                                                              //Image component exists
        Assert.IsTrue(image.sprite != null);
    }

    [UnityTest]
    public IEnumerator _09MainMenuSceneStartButtonWorks()
    {
        SceneManager.LoadScene(TestConstants.MENU_SCENE_NAME);
        yield return null;
        Assert.NotNull(GameObject.Find("StartButton"));
        Button button = GameObject.Find("StartButton").GetComponent<Button>();
        Assert.NotNull(button);                                                             //button component exists
        Assert.IsTrue(button.onClick.GetPersistentMethodName(0) == "LoadGameplayScene");    //button onclick event should call MainMenuController.LoadGameplayScene
        Assert.IsTrue(button.transform.GetComponentInChildren<Text>().text == "Start");     //button object has text and it says "Start" 
        button.onClick.Invoke();                                                            //triggering button on click should load main scene
        yield return null;
        Assert.IsTrue(SceneManager.GetActiveScene().name == TestConstants.GAMEPLAY_INTRO_SCENE_NAME.Split('/')[1]);
    }

    [UnityTest]
    public IEnumerator _10MainMenuSceneQuitButtonExists()   //not sure how to write a test for application.quit
    {
        SceneManager.LoadScene(TestConstants.MENU_SCENE_NAME);
        yield return null;
        Assert.NotNull(GameObject.Find("QuitButton"));
        Button button = GameObject.Find("QuitButton").GetComponent<Button>();
        Assert.NotNull(button);                                                             //button component exists
        Assert.IsTrue(button.onClick.GetPersistentMethodName(0) == "QuitApplication");      //button onclick event should call MainMenuController.QuitApplication
        Assert.IsTrue(button.transform.GetComponentInChildren<Text>().text == "Quit");     //button object has text and it says "Quit" 
    }

    [UnityTest]
    public IEnumerator _11GameplaySceneContainsInGameMenuUIElements()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_SCENE_NAME);
        yield return null;
        Assert.NotNull(GameObject.Find("InGame_Menu"));
        Canvas canvas = GameObject.Find("InGame_Menu").GetComponent<Canvas>();
        Assert.NotNull(canvas);                                                             //scene has a canvas
        Assert.NotNull(canvas.GetComponent<InGameMenuController>());
        Assert.NotNull(Object.FindObjectOfType<EventSystem>());                             //scene has an event system

        //Assert.IsTrue(canvas.transform.childCount == 2);
        Assert.NotNull(canvas.transform.GetChild(0));                                       //canvas has a Pause menu
        Assert.IsFalse(canvas.transform.GetChild(0).gameObject.activeInHierarchy);          //Pause menu is disabled by default
        Assert.NotNull(canvas.transform.GetChild(1));                                       //canvas has a button child (for opening in game menu on mobile)
#if !(UNITY_ANDROID || UNITY_IPHONE)
        Assert.IsFalse(canvas.transform.GetChild(1).gameObject.activeInHierarchy);
#else
        Assert.IsTrue(canvas.transform.GetChild(1).gameObject.activeInHierarchy);
#endif
    }

    [UnityTest]
    public IEnumerator _12InGameMenuContainsButtons()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_SCENE_NAME);
        yield return null;
        Transform container = GameObject.Find("InGame_Menu").transform.GetChild(0);
        Assert.NotNull(container.GetComponent<VerticalLayoutGroup>());

        //InGameMenu has the game title
        Assert.NotNull(container.GetChild(1).GetComponent<Image>());
        Assert.IsTrue(container.GetChild(1).GetComponent<Image>().sprite != null);

        //Pause menu has a resume button
        Assert.NotNull(container.GetChild(1).GetComponent<Button>());
        Assert.IsTrue(container.GetChild(1).name == "ResumeButton");
        Assert.IsTrue(container.GetChild(1).GetComponent<Button>().onClick.GetPersistentMethodName(0) == "ChangeMenuState");

        //Pause menu has a quit button
        Assert.NotNull(container.GetChild(2).GetComponent<Button>());
        Assert.IsTrue(container.GetChild(2).name == "QuitButton");
        Assert.IsTrue(container.GetChild(2).GetComponent<Button>().onClick.GetPersistentMethodName(0) == "BackToMainMenu");
    }


    [UnityTest]
    public IEnumerator _13PauseMenuCanBeEnabledAndDisabled()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_SCENE_NAME);
        yield return null;
        InGameMenuController menuController = GameObject.Find("InGame_Menu").GetComponent<InGameMenuController>();
        Assert.IsTrue(menuController.transform.GetChild(0).name == "PauseMenu");
#if !(UNITY_ANDROID || UNITY_IPHONE)
        menuController.ChangeMenuState(true);
        Assert.IsTrue(menuController.transform.GetChild(0).gameObject.activeInHierarchy);
        Assert.IsFalse(menuController.transform.GetChild(1).gameObject.activeInHierarchy);
        menuController.ChangeMenuState(false);
        Assert.IsFalse(menuController.transform.GetChild(0).gameObject.activeInHierarchy);
        Assert.IsFalse(menuController.transform.GetChild(1).gameObject.activeInHierarchy);
#else
        GameObject mobileInputCanvas = GameObject.Find("MobileInputCanvas");

        menuController.ChangeMenuState(true);
        Assert.IsTrue(menuController.transform.GetChild(0).gameObject.activeInHierarchy);
        Assert.IsFalse(menuController.transform.GetChild(1).gameObject.activeInHierarchy);
        Assert.IsFalse(mobileInputCanvas.activeInHierarchy);

        menuController.ChangeMenuState(false);
        Assert.IsFalse(menuController.transform.GetChild(0).gameObject.activeInHierarchy);
        Assert.IsTrue(menuController.transform.GetChild(1).gameObject.activeInHierarchy);
        Assert.IsTrue(mobileInputCanvas.activeInHierarchy);
#endif
    }

    [UnityTest]
    public IEnumerator _14PauseMenuChangesGameManagerGameState()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_SCENE_NAME);
        yield return null;
        InGameMenuController menuController = GameObject.Find("InGame_Menu").GetComponent<InGameMenuController>();
        menuController.ChangeMenuState(true);
        Assert.IsTrue(GameManager.IsPaused);
        Assert.AreEqual(Time.timeScale, 0.0f, 0.01f);
        menuController.ChangeMenuState(false);
        Assert.IsFalse(GameManager.IsPaused);
        Assert.AreEqual(Time.timeScale, 1.0f, 0.01f);
    }

    [UnityTest]
    public IEnumerator _15MobilePauseMenuButtonActivatesPauseMenu()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_SCENE_NAME);
        yield return null;
        GameObject pauseMenu = GameObject.Find("InGame_Menu").transform.GetChild(0).gameObject;
        GameObject pauseMenuButton = GameObject.Find("InGame_Menu").transform.GetChild(1).gameObject;

        Assert.NotNull(pauseMenuButton);
        Button button = pauseMenuButton.GetComponent<Button>();
        Assert.NotNull(button);                                                             //button component exists
        Assert.IsTrue(button.onClick.GetPersistentMethodName(0) == "ChangeMenuState");      //button onclick event should call ChangeMenuState
        Assert.IsTrue(button.onClick.GetPersistentTarget(0).name == "InGame_Menu");
        button.onClick.Invoke();                                                            //triggering button on click should load main scene
        Assert.IsTrue(pauseMenu.activeInHierarchy);                                         //pause menu should appear

#if (UNITY_ANDROID || UNITY_IPHONE)
        Assert.IsFalse(pauseMenuButton.activeInHierarchy);                                  //Pause button should disappear
#endif
    }

    /* //instability
    [UnityTest]
    public IEnumerator _16InGameScoreCounterChangesWhenScoreChanges()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_SCENE_NAME);
        yield return null;
        Assert.NotNull(GameObject.Find("InGame_Menu"));
        Canvas canvas = GameObject.Find("InGame_Menu").GetComponent<Canvas>();

        Assert.NotNull(canvas.transform.GetChild(2));                                       //canvas has a scorecounter
        Text[] numbers = canvas.transform.GetChild(2).GetComponentsInChildren<Text>();
        Assert.IsTrue(numbers.Length == 7);
        GameManager.AddAsteroidToScore(0);
        yield return null;
        yield return new WaitForSeconds(1.0f);
        Assert.IsTrue(numbers[0].text == "0" && numbers[1].text == "0" && numbers[2].text == "0" && numbers[3].text == "1" && numbers[4].text == "0" && numbers[5].text == "0" && numbers[6].text == "0", numbers[0].text + numbers[1].text + numbers[2].text + numbers[3].text + numbers[4].text + numbers[5].text + numbers[6].text + ", expected 0001000");
    }
    */

    [UnityTest]
    public IEnumerator _17InGameMenuLivesControllerPlaysAnimationsWhenLifeCountIsChanged()
    {
        Transform inGameMenu = Object.Instantiate(Prefab_InGameMenu, Vector3.zero, Quaternion.identity).transform.GetChild(3);
        Animator[] lives = inGameMenu.GetComponentsInChildren<Animator>();
        Assert.IsTrue(lives.Length == 3);

        yield return null;
        inGameMenu.GetComponent<LifeCounter>().SetLives(2);     // 2/3 lives
        yield return null;
        for (int i = 0; i < lives.Length; i++)
        {
            if (i >= 2)
            {
                Assert.IsTrue(lives[i].enabled == true);
                Assert.IsTrue(lives[i].GetCurrentAnimatorStateInfo(0).IsName("RemoveLife"));    
            }
            else
            {
                Assert.IsTrue(lives[i].enabled == false);
            }
        }

        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < lives.Length; i++)
            Assert.IsTrue(lives[i].enabled == false); //state machine behaviour should have disabled the animator after the animations have been finished
        inGameMenu.GetComponent<LifeCounter>().SetLives(0);     // 0/3 lives from 2/3 lives, 2 animations should play
        yield return null;
        for (int i = 0; i < lives.Length; i++)
        {
            if (i < lives.Length - 1)
            {
                Assert.IsTrue(lives[i].enabled == true);
                Assert.IsTrue(lives[i].GetCurrentAnimatorStateInfo(0).IsName("RemoveLife"));
            }
            else
            {
                Assert.IsTrue(lives[i].enabled == false);

            }
        }

        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < lives.Length; i++)
            Assert.IsTrue(lives[i].enabled == false); //state machine behaviour should have disabled the animator after the animations have been finished
        inGameMenu.GetComponent<LifeCounter>().SetLives(3);     // 3/3 lives from 0/3 lives, 3 animations should play
        yield return null;
        for (int i = 0; i < lives.Length; i++)
        {
            Assert.IsTrue(lives[i].enabled == true);
            Assert.IsTrue(lives[i].GetCurrentAnimatorStateInfo(0).IsName("RecoverLife"));    //spawn animation plays
        }
    }


    [UnityTest]
    public IEnumerator _18PopupsMoveAndFadeOut()    //rect transform anchored position, text.text, text color, destroy
    {
        GameManager.InitializeTestingEnvironment(true, false, true, false, false);
        SceneManager.LoadScene(TestConstants.GAMEPLAY_SCENE_NAME);
        yield return null;
        Assert.NotNull(PopupManager.instance);
        PopupManager.instance.InstantiatePopup(Vector2.zero, "TestingTesting");
        Vector3 startingPos = PopupManager.instance.transform.GetChild(0).position;
        float dist = Vector3.SqrMagnitude(startingPos - new Vector3(PopupManager.instance.transform.root.GetComponent<RectTransform>().rect.width / 2.0f * PopupManager.instance.transform.root.GetComponent<Canvas>().scaleFactor, PopupManager.instance.transform.root.GetComponent<RectTransform>().rect.height / 2.0f * PopupManager.instance.transform.root.GetComponent<Canvas>().scaleFactor));
        Assert.IsTrue(dist < 0.02f, "Position should be at the centre of the screen, distance from centre: " + dist + ". (Previously failed due to Game View width/height being an uneven number, current resolution: " + PopupManager.instance.transform.root.GetComponent<RectTransform>().rect.width  + "x" + PopupManager.instance.transform.root.GetComponent<RectTransform>().rect.height + ")"); //text position should be in the middle of the screen
        Assert.IsTrue(PopupManager.instance.transform.GetChild(0).GetComponent<Text>().text == "TestingTesting");
        //Always wait for at least 2 frames. If delta time is high enough then iterations will be 1 or 0. MovingText.Update() doesn't start updating until the next frame
        //so if there's only frame skipped then object will still be in the same place.
        yield return GenericTestMethods.SkipFrames(2, 0.1f);
        Assert.IsTrue(PopupManager.instance.transform.GetChild(0).position != startingPos);
        Assert.IsTrue(Mathf.Abs(PopupManager.instance.transform.GetChild(0).position.x - startingPos.x) <= Mathf.Epsilon);
        yield return new WaitForSeconds(1f);
        Assert.IsTrue(PopupManager.instance.transform.GetChild(0).GetComponent<Text>().color.a < 1.0f);
        yield return new WaitForSeconds(1.0f);
        Assert.IsNull(PopupManager.instance.transform.GetComponentInChildren<MovingText>());
    }
}