﻿#pragma warning disable IDE1006 // Naming Styles
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Linq;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class Gameplay_Intro_Tests : TestTemplate {
    //List of prefabs
    GameObject Prefab_Gameplay_Intro_Timeline           => GenericTestMethods.GetPrefab("Gameplay_Intro_Timeline");
    GameObject Prefab_Gameplay_Intro_Perspective_Camera => GenericTestMethods.GetPrefab("Gameplay_Intro_Perspective_Camera");
    GameObject Prefab_Gameplay_Intro_Scene_Loader       => GenericTestMethods.GetPrefab("Gameplay_Intro_Scene_Loader");
    GameObject Prefab_Gameplay_Intro_Spaceship          => GenericTestMethods.GetPrefab("Gameplay_Intro_Spaceship");
    GameObject Prefab_Intro_Audio_Listener              => GenericTestMethods.GetPrefab("Intro_Audio_Listener");
    GameObject Prefab_Intro_Skipper     => GenericTestMethods.GetPrefab("Intro_Skipper");
    GameObject Prefab_Gameplay_Camera   => GenericTestMethods.GetPrefab("Gameplay_Camera");

    [Test]
    public void _00PrefabsExist()
    {
        Assert.NotNull(Prefab_Gameplay_Intro_Timeline,              "Missing prefab.");
        Assert.NotNull(Prefab_Gameplay_Intro_Perspective_Camera,    "Missing prefab.");
        Assert.NotNull(Prefab_Gameplay_Intro_Scene_Loader,          "Missing prefab.");
        Assert.NotNull(Prefab_Gameplay_Intro_Spaceship,             "Missing prefab.");
        Assert.NotNull(Prefab_Intro_Skipper,                        "Missing prefab.");
        Assert.NotNull(Prefab_Intro_Audio_Listener,                 "Missing prefab.");
        Assert.NotNull(Prefab_Gameplay_Camera,                      "Missing prefab.");
    }


    [UnityTest]
    public IEnumerator _01SceneExists()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        Assert.IsTrue(SceneManager.GetActiveScene().name == TestConstants.GAMEPLAY_INTRO_SCENE_NAME.Split('/')[1], "Could not find " + TestConstants.GAMEPLAY_INTRO_SCENE_NAME + " scene.");
    }

    [Test]
    public void _02ValidateTimelinePrefabSetup()
    {
        PlayableDirector director = Prefab_Gameplay_Intro_Timeline.GetComponent<PlayableDirector>();
        Assert.NotNull(director);
        Assert.NotNull(director.playableAsset);
        Assert.IsTrue(director.timeUpdateMode == DirectorUpdateMode.GameTime);
        Assert.IsTrue(director.playOnAwake);
        Assert.IsTrue(director.extrapolationMode == DirectorWrapMode.Hold);
        Assert.IsTrue(director.initialTime < 0.01f);
    }

    [Test]
    public void _03ValidatePerspectiveCameraPrefabSetup()
    {
        Camera camera = Prefab_Gameplay_Intro_Perspective_Camera.GetComponent<Camera>();
        Assert.NotNull(camera);
        Assert.IsTrue(camera.clearFlags == CameraClearFlags.SolidColor, "Invalid Camera Clear Flags setting, should be CameraClearFlags.SolidColor");
        Assert.IsTrue(camera.backgroundColor.Equals(Color.black), "Invalid Camera Background color setting, should be Black");
        Assert.IsTrue(camera.orthographic == false, "Invalid Camera Projection setting, should be Perspective");
        Assert.NotNull(camera.transform.GetChild(0));
        Assert.NotNull(camera.transform.GetChild(0).GetChild(0).GetComponent<ParticleSystem>(), "Missing one of two child particle systems.");
        Assert.NotNull(camera.transform.GetChild(0).GetChild(1).GetComponent<ParticleSystem>(), "Missing one of two child particle systems.");
    }

    [Test]
    public void _04ValidateSceneLoaderPrefabSetup()
    {
        SceneLoader loader = Prefab_Gameplay_Intro_Scene_Loader.GetComponent<SceneLoader>();
        Assert.NotNull(loader);
        Assert.IsTrue(loader.enabled);
        Assert.IsTrue(loader.gameObject.activeSelf, "Prefab gameobject should be active (active status is overriden in scene).");
    }

    [Test]
    public void _05ValidateSpaceshipPrefabSetup()
    {
        GameObject spaceship = Prefab_Gameplay_Intro_Spaceship;
        Assert.NotNull(spaceship);
        Assert.NotNull(spaceship.GetComponent<MeshFilter>());
        Assert.NotNull(spaceship.GetComponent<MeshRenderer>());
        Assert.NotNull(spaceship.GetComponent<Animator>());
        Assert.NotNull(spaceship.GetComponent<Animator>().runtimeAnimatorController);
        Assert.IsTrue(spaceship.GetComponent<Animator>().updateMode == AnimatorUpdateMode.Normal);
        Assert.IsTrue(spaceship.GetComponent<Animator>().cullingMode == AnimatorCullingMode.AlwaysAnimate);
        Assert.NotNull(spaceship.transform.GetChild(0).GetComponent<Light>());
        Assert.NotNull(spaceship.transform.GetChild(1).GetComponent<ParticleSystem>(), "Missing engine trail particle system.");
        Assert.NotNull(spaceship.transform.GetChild(1).GetChild(0).GetComponent<Light>(), "Missing engine trail backlight.");
    }

    [Test]
    public void _06ValidateAudioListenerPrefabSetup()
    {
        GameObject listener = Prefab_Intro_Audio_Listener;
        Assert.NotNull(listener);
        Assert.NotNull(listener.GetComponent<AudioListener>());
        Assert.NotNull(listener.GetComponent<AudioSource>());
        Assert.Null(listener.GetComponent<AudioSource>().clip);
        Assert.IsFalse(listener.GetComponent<AudioSource>().playOnAwake);
        Assert.IsFalse(listener.GetComponent<AudioSource>().mute);
        Assert.IsTrue(listener.GetComponent<AudioSource>().volume > 0.01f);
    }

    [Test]
    public void _07ValidateIntroSkipperPrefabSetup()
    {
        GameObject skipper = Prefab_Intro_Skipper;
        Assert.NotNull(skipper);
        Assert.NotNull(skipper.GetComponent<IntroSkipper>());
    }

    [UnityTest]
    public IEnumerator _08IntroSceneContainsRequiredPrefabs()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        Assert.NotNull(GameObject.Find(Prefab_Intro_Skipper.name), "Missing prefab in scene.");
        Assert.NotNull(GameObject.Find(Prefab_Gameplay_Camera.name), "Missing prefab in scene.");
        Assert.NotNull(GameObject.Find(Prefab_Intro_Audio_Listener.name), "Missing prefab in scene.");
        Assert.NotNull(GameObject.Find(Prefab_Gameplay_Intro_Timeline.name), "Missing prefab in scene.");
        Assert.NotNull(GameObject.Find(Prefab_Gameplay_Intro_Spaceship.name), "Missing prefab in scene.");
        Assert.NotNull(GameObject.Find(Prefab_Gameplay_Intro_Perspective_Camera.name), "Missing prefab in scene.");
        //TODO check by prefab reference while in Editor, not by name
    }

    [UnityTest]
    public IEnumerator _09IntroSkipperSuccessfullyLoadsNextScene()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        Scene initialScene = SceneManager.GetActiveScene();
        int initialBuildIndex = initialScene.buildIndex;
        GameObject skipper = GameObject.Find("Intro_Skipper");
        Assert.NotNull(skipper, "Missing IntroSkipper.");
        IntroSkipper.Continue();
        yield return null;
        Assert.IsTrue(SceneManager.GetActiveScene() != initialScene, "IntroSkipper failed to load the next scene.");
        Assert.IsTrue(SceneManager.GetActiveScene().buildIndex == initialBuildIndex + 1, "IntroSkipper didn't load the correct scene.");
    }

    [UnityTest]
    public IEnumerator _10TimelinePlaysOnAwake()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        PlayableDirector director = GameObject.Find("Gameplay_Intro_Timeline").transform.GetComponent<PlayableDirector>();
        Assert.IsTrue(director.playOnAwake, "Play On Awake should be enabled.");
        Assert.IsTrue(director.state == PlayState.Playing, "Timeline is not playing.");
    }

    [UnityTest]
    public IEnumerator _11TimelineTrackBindingsReferenceObjectsInScene()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        PlayableDirector director = GameObject.Find("Gameplay_Intro_Timeline").transform.GetComponent<PlayableDirector>();

        //Total of 9 tracks in the Timeline asset
        Assert.IsTrue(director.playableGraph.GetOutputCount() == 9);

        //Gameplay_Camera Activation Track
        Assert.NotNull(director.playableGraph.GetOutput(0));
        Assert.NotNull((director.playableAsset as TimelineAsset).GetOutputTrack(0));
        Assert.IsTrue(director.GetGenericBinding((director.playableAsset as TimelineAsset).GetOutputTrack(0)) == GameObject.Find("Gameplay_Camera"));

        //Gameplay_Camera child object Canvas Image Animation Track
        Assert.NotNull(director.playableGraph.GetOutput(1));
        Assert.NotNull((director.playableAsset as TimelineAsset).GetOutputTrack(1));
        Assert.IsTrue(director.GetGenericBinding((director.playableAsset as TimelineAsset).GetOutputTrack(1)) == GameObject.Find("Image").GetComponent<Animator>());

        //Gameplay_Camera Animation Track
        Assert.NotNull(director.playableGraph.GetOutput(2));
        Assert.NotNull((director.playableAsset as TimelineAsset).GetOutputTrack(2));
        Assert.IsTrue(director.GetGenericBinding((director.playableAsset as TimelineAsset).GetOutputTrack(2)) == GameObject.Find("Gameplay_Camera").GetComponent<Animator>());

        //Gameplay_Camera child object Stars ParticleSystem Activation Track
        Assert.NotNull(director.playableGraph.GetOutput(3));
        Assert.NotNull((director.playableAsset as TimelineAsset).GetOutputTrack(3));
        Assert.IsTrue(director.GetGenericBinding((director.playableAsset as TimelineAsset).GetOutputTrack(3)) == GameObject.Find("Gameplay_Camera").transform.GetChild(0).gameObject);

        //Gameplay_Intro_Perspective_Camera Activation Track
        Assert.NotNull(director.playableGraph.GetOutput(4));
        Assert.NotNull((director.playableAsset as TimelineAsset).GetOutputTrack(4));
        Assert.IsTrue(director.GetGenericBinding((director.playableAsset as TimelineAsset).GetOutputTrack(4)) == GameObject.Find("Gameplay_Intro_Perspective_Camera"));

        //Gameplay_Intro_Spaceship Activation Track
        Assert.NotNull(director.playableGraph.GetOutput(5));
        Assert.NotNull((director.playableAsset as TimelineAsset).GetOutputTrack(5));
        Assert.IsTrue(director.GetGenericBinding((director.playableAsset as TimelineAsset).GetOutputTrack(5)) == GameObject.Find("Gameplay_Intro_Spaceship"));

        //Gameplay_Intro_Spaceship Animation Track
        Assert.NotNull(director.playableGraph.GetOutput(6));
        Assert.NotNull((director.playableAsset as TimelineAsset).GetOutputTrack(6));
        Assert.IsTrue(director.GetGenericBinding((director.playableAsset as TimelineAsset).GetOutputTrack(6)) == GameObject.Find("Gameplay_Intro_Spaceship").GetComponent<Animator>());

        //Gameplay_Intro_Scene_Loader Activation Track
        Assert.NotNull(director.playableGraph.GetOutput(7));
        Assert.NotNull((director.playableAsset as TimelineAsset).GetOutputTrack(7));
        Assert.NotNull((director.playableAsset as TimelineAsset).GetOutputTrack(7));
        Assert.NotNull((director.GetGenericBinding((director.playableAsset as TimelineAsset).GetOutputTrack(7)) as GameObject).GetComponent<SceneLoader>());
        Assert.IsTrue((director.GetGenericBinding((director.playableAsset as TimelineAsset).GetOutputTrack(7)) as GameObject).activeInHierarchy == false);
        Assert.IsTrue(director.GetGenericBinding((director.playableAsset as TimelineAsset).GetOutputTrack(7)).name == "Gameplay_Intro_Scene_Loader");

        //Intro_Audio_Listener Audio Track
        Assert.NotNull(director.playableGraph.GetOutput(8));
        Assert.NotNull((director.playableAsset as TimelineAsset).GetOutputTrack(8));
        Assert.IsTrue(director.GetGenericBinding((director.playableAsset as TimelineAsset).GetOutputTrack(8)) == GameObject.Find("Intro_Audio_Listener").GetComponent<AudioSource>());
    }

    [UnityTest]
    public IEnumerator _12TimelineActivationTrackActivatesAndDeactivatesGameplayCamera()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        PlayableDirector director = GameObject.Find("Gameplay_Intro_Timeline").transform.GetComponent<PlayableDirector>();

        TrackAsset track = (director.playableAsset as TimelineAsset).GetOutputTrack(0);
        Assert.NotNull(track);
        Assert.IsTrue(track.name == "Gameplay_Camera_Activation", "Can't find Gameplay_Camera_Activation track. Actual track name: " + track.name);

        GameObject camera = director.GetGenericBinding(track) as GameObject;
        TimelineClip firstClip = track.GetClips().ElementAt(0);
        TimelineClip secondClip = track.GetClips().ElementAt(1);

        director.playableGraph.GetOutput(7).SetUserData(null); //Disable scene loader track to prevent timeline from loading the next scene during the test

        Assert.NotNull(camera);
        Assert.NotNull(firstClip);
        Assert.NotNull(secondClip);

        //Gameplay_Camera Activation Track
        Assert.IsTrue(camera.activeInHierarchy, "Camera is not active at the start of the first activationclip.");
        director.time = firstClip.end;
        yield return null;
        Assert.IsFalse(camera.activeInHierarchy, "Camera should not be active after the activation clip ended.");
        director.time = secondClip.start;
        yield return null;
        Assert.IsTrue(camera.activeInHierarchy, "Camera is not reactivated at the start of the second activation clip.");
        director.time = secondClip.end;
        yield return null;
        Assert.IsFalse(camera.activeInHierarchy, "Camera should not be active after the activation clip ended.");
    }

    [UnityTest]
    public IEnumerator _13TimelineAnimationTrackAnimatesCanvasImage()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        PlayableDirector director = GameObject.Find("Gameplay_Intro_Timeline").transform.GetComponent<PlayableDirector>();

        TrackAsset track = (director.playableAsset as TimelineAsset).GetOutputTrack(1);
        Assert.NotNull(track);
        Assert.IsTrue(track.name == "Background_Animation", "Can't find Background_Animation track. Actual track name: " + track.name);
        Assert.IsTrue((track as AnimationTrack).inClipMode, "Track clip mode should be enabled.");

        Image image = (director.GetGenericBinding(track) as Animator).GetComponent<Image>();
        TimelineClip firstClip = track.GetClips().ElementAt(0);
        TimelineClip secondClip = track.GetClips().ElementAt(1);

        director.playableGraph.GetOutput(7).SetUserData(null); //Disable scene loader track to prevent timeline from loading the next scene during the test

        Assert.NotNull(director);
        Assert.NotNull(image);
        Assert.NotNull(firstClip);
        Assert.NotNull(secondClip);

        float alphaSampleTimelineBeginning = image.color.a;

        //Take animated property samples at the start, middle and end of the animation clip
        director.time = firstClip.start;
        yield return null;
        float firstClipAlphaSample_Start = image.color.a;
        director.time = (firstClip.start + firstClip.end) / 2.0f;
        yield return null;
        float firstClipAlphaSample_Middle = image.color.a;
        director.time = firstClip.end;
        yield return null;
        float firstClipAlphaSample_End = image.color.a;
        Assert.AreEqual(alphaSampleTimelineBeginning, firstClipAlphaSample_Start, 0.05f, "Alpha value at the start of the timeline and at the start of the clip should be the same.");
        Assert.IsTrue(firstClipAlphaSample_Start < firstClipAlphaSample_Middle && firstClipAlphaSample_Middle < firstClipAlphaSample_End, "Alpha value not animated as expected.");
        Assert.AreEqual(firstClipAlphaSample_End, 1.0f, 0.05f, "Alpha value at the end of the first clip should be 1.0f.");

        //Take animated property samples at the start, middle and end of the animation clip
        director.time = secondClip.start;
        yield return null;
        float secondClipAlphaSample_Start = image.color.a;
        director.time = (secondClip.start + secondClip.end) / 2.0f;
        yield return null;
        float secondClipAlphaSample_Middle = image.color.a;
        director.time = secondClip.end;
        yield return null;
        float secondClipAlphaSample_End = image.color.a;
        Assert.AreEqual(firstClipAlphaSample_End, secondClipAlphaSample_Start, 0.05f, "Alpha value at the start of the second clip and at the end of the first clip should be the same.");
        Assert.IsTrue(secondClipAlphaSample_Start > secondClipAlphaSample_Middle && secondClipAlphaSample_Middle > secondClipAlphaSample_End, "Alpha value not animated as expected.");
        Assert.AreEqual(secondClipAlphaSample_End, 0.0f, 0.05f, "Alpha value at the end of the second clip should be 0.0f.");

        Assert.AreEqual(alphaSampleTimelineBeginning, secondClipAlphaSample_End, 0.02f, "Alpha value should be equal at the start and the end of the track.");
    }

    [UnityTest]
    public IEnumerator _14TimelineAnimationTrackAnimatesCameraRotation()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        PlayableDirector director = GameObject.Find("Gameplay_Intro_Timeline").transform.GetComponent<PlayableDirector>();

        TrackAsset track = (director.playableAsset as TimelineAsset).GetOutputTrack(2);
        Assert.NotNull(track);
        Assert.IsTrue(track.name == "Gameplay_Camera_Activation", "Can't find Gameplay_Camera_Activation track. Actual track name: " + track.name);
        Assert.IsTrue((track as AnimationTrack).inClipMode, "Track clip mode should be enabled.");

        Transform camera = (director.GetGenericBinding(track) as Animator).transform;
        TimelineClip firstClip = track.GetClips().ElementAt(0);
        TimelineClip secondClip = track.GetClips().ElementAt(1);
        director.playableGraph.GetOutput(7).SetUserData(null); //Disable scene loader track to prevent timeline from loading the next scene during the test

        Assert.NotNull(director);
        Assert.NotNull(camera);
        Assert.NotNull(firstClip);
        Assert.NotNull(secondClip);

        /*
        float rotationSampleTimelineBeginning = camera.eulerAngles.x;

        //Take animated property samples at the start, middle and end of the animation clip
        director.time = firstClip.start;
        yield return null;
        float firstClipRotationSample_Start = camera.eulerAngles.x;
        director.time = (firstClip.start + firstClip.end) / 2.0f;
        yield return null;
        float firstClipRotationSample_Middle = camera.eulerAngles.x;
        director.time = firstClip.end;
        yield return null;
        float firstClipRotationSample_End = camera.eulerAngles.x;
        Assert.AreEqual(rotationSampleTimelineBeginning, firstClipRotationSample_Start, 0.05f, "Rotation.x value at the start of the timeline and at the start of the clip should be the same.");
        Assert.IsTrue(firstClipRotationSample_Start > firstClipRotationSample_Middle && firstClipRotationSample_Middle > firstClipRotationSample_End, "Rotation.x value not animated as expected.");

        //Take animated property samples at the start, middle and end of the animation clip
        director.time = secondClip.start;
        yield return null;
        float secondClipRotationSample_Start = camera.eulerAngles.x;
        director.time = (secondClip.start + secondClip.end) / 2.0f;
        yield return null;
        float secondClipRotationSample_Middle = camera.eulerAngles.x;
        director.time = secondClip.end;
        yield return null;
        float secondClipRotationSample_End = camera.eulerAngles.x;
        Assert.AreEqual(firstClipRotationSample_End, secondClipRotationSample_Start, 0.05f, "Rotation.x value at the start of the second clip and at the end of the first clip should be the same.");
        Assert.IsTrue(secondClipRotationSample_Start < secondClipRotationSample_Middle && secondClipRotationSample_Middle < secondClipRotationSample_End, "Rotation.x value not animated as expected.");

        Assert.AreEqual(rotationSampleTimelineBeginning, secondClipRotationSample_End, 0.02f, "Rotation.x value should be equal at the start and the end of the track.");
        */
    }

    [UnityTest]
    public IEnumerator _15TimelineActivationTrackActivateAndDeactivateParticleSystems()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        PlayableDirector director = GameObject.Find("Gameplay_Intro_Timeline").transform.GetComponent<PlayableDirector>();

        TrackAsset track = (director.playableAsset as TimelineAsset).GetOutputTrack(3);
        Assert.NotNull(track);
        Assert.IsTrue(track.name == "Star_Particles_Activation", "Can't find Star_Particles_Activation track. Actual track name: " + track.name);
        ParticleSystem particleSystem = (director.GetGenericBinding(track) as GameObject).GetComponent<ParticleSystem>();
        TimelineClip clip = track.GetClips().First();

        director.playableGraph.GetOutput(7).SetUserData(null); //Disable scene loader track to prevent timeline from loading the next scene during the test
        Assert.NotNull(particleSystem);
        Assert.NotNull(clip);
        yield return null;
        Assert.IsFalse(particleSystem.gameObject.activeInHierarchy, "ParticleSystem should not be active at the start of the timeline track.");
        Assert.IsTrue(particleSystem.particleCount == 0, "No particles should exist at the moment.");
        director.time = clip.start;
        yield return null;
        Assert.IsTrue(particleSystem.gameObject.activeInHierarchy, "ParticleSystem is not active at the start of the activation clip.");
        Assert.IsTrue(particleSystem.particleCount > 0, "Particles should be spawning at the start of the clip.");
        director.time = clip.end;
        yield return null;
        Assert.IsFalse(particleSystem.gameObject.activeInHierarchy, "ParticleSystem should not be active after the activation clip ended.");
        Assert.IsTrue(particleSystem.particleCount == 0, "No particles should exist at the moment.");
    }

    [UnityTest]
    public IEnumerator _16TimelineActivationTrackActivateAndDeactivatesPerspectiveCamera()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        PlayableDirector director = GameObject.Find("Gameplay_Intro_Timeline").transform.GetComponent<PlayableDirector>();
        TrackAsset track = (director.playableAsset as TimelineAsset).GetOutputTrack(4);
        Assert.NotNull(track);
        Assert.IsTrue(track.name == "Perspective_Camera_Activation", "Can't find Perspective_Camera_Activation track. Actual track name: " + track.name);
        GameObject camera =director.GetGenericBinding(track) as GameObject;
        TimelineClip clip = track.GetClips().First();
        director.playableGraph.GetOutput(7).SetUserData(null); //Disable scene loader track to prevent timeline from loading the next scene during the test
        Assert.NotNull(camera);
        Assert.NotNull(clip);
        yield return null;
        Assert.IsFalse(camera.gameObject.activeInHierarchy, "Perspective Camera should not be active at the start of the timeline track.");
        director.time = clip.start;
        yield return null;
        Assert.IsTrue(camera.gameObject.activeInHierarchy, "Perspective Camera is not active at the start of the activation clip.");
        director.time = clip.end;
        yield return null;
        Assert.IsFalse(camera.gameObject.activeInHierarchy, "Perspective Camera should not be active after the activation clip ended.");
    }

    [UnityTest]
    public IEnumerator _17TimelineActivationTrackActivateAndDeactivatesSpaceship()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        PlayableDirector director = GameObject.Find("Gameplay_Intro_Timeline").transform.GetComponent<PlayableDirector>();
        TrackAsset track = (director.playableAsset as TimelineAsset).GetOutputTrack(5);
        Assert.NotNull(track);
        Assert.IsTrue(track.name == "Spaceship_Activation", "Can't find Spaceship_Activation track. Actual track name: " + track.name);
        GameObject spaceship = director.GetGenericBinding(track) as GameObject;

        TimelineClip clip = track.GetClips().First();
        director.playableGraph.GetOutput(7).SetUserData(null); //Disable scene loader track to prevent timeline from loading the next scene during the test
        Assert.NotNull(clip);
        Assert.NotNull(spaceship);
        yield return null;
        Assert.IsFalse(spaceship.gameObject.activeInHierarchy, "Spaceship should not be active at the start of the timeline track.");
        director.time = clip.start;
        yield return null;
        Assert.IsTrue(spaceship.gameObject.activeInHierarchy, "Spaceship is not active at the start of the activation clip.");
        director.time = clip.end;
        yield return null;
        Assert.IsFalse(spaceship.gameObject.activeInHierarchy, "Spaceship should not be active after the activation clip ended.");
    }

    [UnityTest]
    public IEnumerator _17TimelineAnimationTrackAnimatesSpaceship()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        PlayableDirector director = GameObject.Find("Gameplay_Intro_Timeline").transform.GetComponent<PlayableDirector>();
        TrackAsset track = (director.playableAsset as TimelineAsset).GetOutputTrack(6);
        Assert.NotNull(track);
        Assert.IsTrue(track.name == "Spaceship_Animation", "Can't find Spaceship_Animation track. Actual track name: " + track.name);
        GameObject spaceship = (director.GetGenericBinding(track) as Animator).gameObject;

        TimelineClip clip = track.GetClips().First();
        director.playableGraph.GetOutput(7).SetUserData(null); //Disable scene loader track to prevent timeline from loading the next scene during the test
        Assert.NotNull(clip);
        Assert.NotNull(spaceship);
        //...
    }


    [UnityTest]
    public IEnumerator _18TimelineActivationTrackLoadsGameplayScene()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        PlayableDirector director = GameObject.Find("Gameplay_Intro_Timeline").transform.GetComponent<PlayableDirector>();
        TrackAsset track = (director.playableAsset as TimelineAsset).GetOutputTrack(7);
        Assert.NotNull(track);
        Assert.IsTrue(track.name == "Scene_Loader_Activation", "Can't find Scene_Loader_Activation track. Actual track name: " + track.name);
        GameObject loader = director.GetGenericBinding(track) as GameObject;
        TimelineClip clip = track.GetClips().First();
        Assert.NotNull(clip);
        Assert.NotNull(loader);

        Scene initialScene = SceneManager.GetActiveScene();
        Assert.IsFalse(loader.gameObject.activeInHierarchy, "Scene Loader should not be active at the start of the timeline track.");
        director.time = clip.start;
        yield return null;
        Assert.IsTrue(SceneManager.GetActiveScene() != initialScene, "Scene Loader failed to load the next scene.");
        Assert.IsTrue(SceneManager.GetActiveScene().name == TestConstants.GAMEPLAY_SCENE_NAME.Split('/')[1], "Failed to load Gameplay scene.");
    }

    [UnityTest]
    public IEnumerator _19TimelineAudioTrackPlaysAudio()
    {
        AudioListener.volume = 1.0f;
        SceneManager.LoadScene(TestConstants.GAMEPLAY_INTRO_SCENE_NAME);
        yield return null;
        PlayableDirector director = GameObject.Find("Gameplay_Intro_Timeline").transform.GetComponent<PlayableDirector>();
        TrackAsset track = (director.playableAsset as TimelineAsset).GetOutputTrack(8);
        Assert.NotNull(track);
        Assert.IsTrue(track.name == "Audio_Track", "Can't find Audio_Track track. Actual track name: " + track.name);
        AudioSource audioSource = director.GetGenericBinding(track) as AudioSource;
        director.playableGraph.GetOutput(7).SetUserData(null); //Disable scene loader track to prevent timeline from loading the next scene during the tes
        Assert.NotNull(audioSource);

        TimelineClip firstClip  = track.GetClips().ElementAt(0);
        TimelineClip secondClip = track.GetClips().ElementAt(1);
        TimelineClip thirdClip  = track.GetClips().ElementAt(2);
        TimelineClip fourthClip = track.GetClips().ElementAt(3);

        Assert.NotNull(firstClip, "Can't find Audio clip.");
        Assert.NotNull((firstClip.asset as AudioPlayableAsset).clip, "Can't find Audio clip.");
        Assert.IsTrue((firstClip.asset as AudioPlayableAsset).clip.name == "sci-fi_vehicle_pass_01", "Can't find Audio Clip by name. Actual track name: " + (firstClip.asset as AudioPlayableAsset).clip.name);
        Assert.NotNull(secondClip, "Can't find Audio clip.");
        Assert.NotNull((secondClip.asset as AudioPlayableAsset).clip, "Can't find Audio clip.");
        Assert.IsTrue((secondClip.asset as AudioPlayableAsset).clip.name == "sci-fi_vehicle_spaceship_jet_engine_loop1", "Can't find Audio Clip by name. Actual track name: " + (secondClip.asset as AudioPlayableAsset).clip.name);
        Assert.NotNull(thirdClip, "Can't find Audio clip.");
        Assert.NotNull((thirdClip.asset as AudioPlayableAsset).clip, "Can't find Audio clip.");
        Assert.IsTrue((thirdClip.asset as AudioPlayableAsset).clip.name == "sci-fi_vehicle_pass_02", "Can't find Audio Clip by name. Actual track name: " + (thirdClip.asset as AudioPlayableAsset).clip.name);
        Assert.NotNull(fourthClip, "Can't find Audio clip.");
        Assert.NotNull((fourthClip.asset as AudioPlayableAsset).clip, "Can't find Audio clip.");
        Assert.IsTrue((fourthClip.asset as AudioPlayableAsset).clip.name == "time_warp_reverse_high_06", "Can't find Audio Clip by name. Actual track name: " + (fourthClip.asset as AudioPlayableAsset).clip.name);

        //audio clip blending
        Assert.IsTrue(firstClip.end > secondClip.start && secondClip.end > thirdClip.start && thirdClip.end > fourthClip.start);
        Assert.AreEqual(firstClip.easeOutDuration, secondClip.easeInDuration, 0.01f);
        Assert.AreEqual(secondClip.easeOutDuration, thirdClip.easeInDuration, 0.01f);
        Assert.AreEqual(thirdClip.easeOutDuration, fourthClip.easeInDuration, 0.01f);

        //TODO: enable when https://fogbugz.unity3d.com/f/cases/1146694/ is fixed, currently there is no way to detect if the audio source is currently being used by a timeline.
        /*
        Assert.IsFalse(audioSource.isPlaying, "Audio should not be playing at the start of the timeline.");
        director.time = firstClip.start;
        yield return null;
        Assert.IsTrue(audioSource.isPlaying, "Audio should be playing at the start of the first audio clip.");
        director.time = secondClip.start;
        yield return null;
        Assert.IsTrue(audioSource.isPlaying, "Audio should be playing at the start of the first audio clip.");
        director.time = thirdClip.start;
        yield return null;
        Assert.IsTrue(audioSource.isPlaying, "Audio should be playing at the start of the first audio clip.");
        director.time = fourthClip.start;
        yield return null;
        Assert.IsTrue(audioSource.isPlaying, "Audio should be playing at the start of the first audio clip.");
        */
    }
}