﻿#pragma warning disable IDE1006 // Naming Styles
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class GameManagerTests : TestTemplate {
    //List of prefabs
    GameObject Prefab_GameManager => GenericTestMethods.GetPrefab("GameManager");
    GameObject Prefab_Asteroid  => GenericTestMethods.GetPrefab("Base_Asteroid");
    GameObject Prefab_MainCamera => GenericTestMethods.GetPrefab("Gameplay_Camera");

    [Test]
    public void _01RequiredLayersExist() {
        Assert.IsTrue(LayerMask.GetMask("Asteroids") != 0);
        Assert.IsTrue(LayerMask.GetMask("Spaceship") != 0);
        Assert.IsTrue(LayerMask.GetMask("Projectiles") != 0);
    }

    [Test]
    public void _02GameManagerPrefabExists()
    {
        Assert.NotNull(Prefab_GameManager);

    }

    [Test]
    public void _02GameManagerPrefabHasRequiredComponentScript()
    {
        Assert.IsNotNull(Prefab_GameManager.GetComponent<GameManager>());
    }

    [UnityTest]
    public IEnumerator _03GameManagerCanSpawnSpaceshipOnLoad()
    {
        Object.Instantiate(Prefab_GameManager).GetComponent<GameManager>();
        GameManager.InitializeTestingEnvironment(true, false, false, false, false);

        yield return null;
        SpaceshipController spaceship = Object.FindObjectOfType<SpaceshipController>();        
        Assert.IsTrue(spaceship != null);
    }

    [UnityTest]
    public IEnumerator _04GameManagerRespawnsSpaceshipAfterItHasBeenDestroyed()
    {
        GameManager gameManager = Object.Instantiate(Prefab_GameManager).GetComponent<GameManager>();
        GameManager.InitializeTestingEnvironment(true, false, false, false, false);
        yield return null;
        gameManager.deaths = 0;
        Assert.IsTrue(GameManager.SpaceshipIsActive());
        Object.DestroyImmediate(Object.FindObjectOfType<SpaceshipController>());
        gameManager.RespawnShip(0.0f);
        yield return null;
        yield return null;
        SpaceshipController spaceship = Object.FindObjectOfType<SpaceshipController>();
        Assert.IsTrue(GameManager.SpaceshipIsActive());
    }

    [UnityTest]
    public IEnumerator _05GameManagerDoesNotRespawnShipAfterThreeDeaths()
    {
        GameManager gameManager = Object.Instantiate(Prefab_GameManager).GetComponent<GameManager>();
        GameManager.InitializeTestingEnvironment(true, false, false, false, false);
        Object.DestroyImmediate(Object.FindObjectOfType<SpaceshipController>());
        gameManager.deaths = 1;
        gameManager.RespawnShip(0.0f);
        yield return null;
        SpaceshipController spaceship = Object.FindObjectOfType<SpaceshipController>();
        Assert.IsTrue(spaceship != null);                                       //ship should still respawn after dying 2 times
        Object.DestroyImmediate(spaceship);
        gameManager.RespawnShip(0.0f);
        yield return null;
        spaceship = Object.FindObjectOfType<SpaceshipController>();
        Assert.IsTrue(!GameManager.SpaceshipIsActive());
    }

    [UnityTest]
    public IEnumerator _06GameManagerSpawnsAsteroids() 
    {
        Object.Instantiate(Prefab_GameManager);
        GameManager.InitializeTestingEnvironment(false, true, false, true, false);

        yield return null;
        AsteroidController[] asteroids = Object.FindObjectsOfType<AsteroidController>();        //find all asteroids in scene
        Assert.IsTrue(asteroids.Length > 0);                                                   
    }

    [UnityTest]
    public IEnumerator _07GameManagerSpawnsAsteroidsOverTime()
    {
        Object.Instantiate(Prefab_MainCamera);
        GameManager gameManager = Object.Instantiate(Prefab_GameManager).GetComponent<GameManager>();
        GameManager.InitializeTestingEnvironment(false, true, false, true, false);

        yield return new WaitForSeconds(gameManager.asteroidSpawnDelay + 0.5f);
        AsteroidController[] asteroids = Object.FindObjectsOfType<AsteroidController>();        //find all asteroids in scene
        Assert.IsTrue(asteroids.Length > 1);
    }


    [Test]
    public void _08GameManagerOnlySpawnsAsteroidsOffscreen()
    {
        GameManager gameManager = Object.Instantiate(Prefab_GameManager).GetComponent<GameManager>();
        GameManager.InitializeTestingEnvironment(false, false, false, false, false);

        Object.Instantiate(Prefab_MainCamera);
        for (int i = 0; i < 100; i++) 
            gameManager.SpawnAsteroids();
        AsteroidController[] asteroids = Object.FindObjectsOfType<AsteroidController>();        //find all asteroids in scene
        foreach (AsteroidController roid in asteroids)
        {
            Vector2 positionOnCamera = Camera.main.WorldToViewportPoint(roid.transform.position);
            //Debug.Log(positionOnCamera.x + " " + positionOnCamera.y);
            Assert.IsTrue(((positionOnCamera.x >= -0.1f && positionOnCamera.x <= 1.1f) && (positionOnCamera.y <= -0.05f || positionOnCamera.y >= 1.05f)) || 
                            ((positionOnCamera.x <= -0.05f || positionOnCamera.x >= 1.05f) && (positionOnCamera.y >= -0.1f && positionOnCamera.y <= 1.1f)));
        }
        
    }

    [Test]
    public void _09GameManagerSpawnsRandomSizeAsteroids()  //instability due to random spawns(extremely small odds of failure) 
    {
        GameManager gameManager = Object.Instantiate(Prefab_GameManager).GetComponent<GameManager>();
        GameManager.InitializeTestingEnvironment(false, false, false, false, false);

        bool small = false;
        bool medium = false;
        bool big = false;
        for (int i = 0; i < 100; i++)
            gameManager.SpawnAsteroids();
        AsteroidController[] asteroids = Object.FindObjectsOfType<AsteroidController>();        //find all asteroids in scene
        foreach (AsteroidController roid in asteroids)
        {
            if (roid.GetSplitCount() == 2)
                small = true;
            else if (roid.GetSplitCount() == 1)
                medium = true;
            else if (roid.GetSplitCount() == 0)
                big = true;
            if (small && medium && big)
                break;
        }

        Assert.IsTrue(small && medium && big);
    }


    [UnityTest]
    public IEnumerator _10GameManagerScoreIsIncreasedAfterAsteroidsAreDestroyed()
    {
        Object.Instantiate(Prefab_GameManager);
        GameManager.InitializeTestingEnvironment(false, false, false, false, false);

        yield return null;
        AsteroidController asteroid = Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity).GetComponent<AsteroidController>();
        int score = GameManager.score;
        asteroid.Split();
        Assert.IsTrue(score != GameManager.score);
        Assert.IsTrue(GameManager.score == 1000);

        yield return null;
        score = GameManager.score;
        AsteroidController[] asteroids = Object.FindObjectsOfType<AsteroidController>();        //find all asteroids in scene
        foreach(AsteroidController ast in asteroids)
            ast.Split();
        Assert.IsTrue(score != GameManager.score);
        Assert.IsTrue(GameManager.score == 2000);

        yield return null;
        score = GameManager.score;
        asteroids = Object.FindObjectsOfType<AsteroidController>();        //find all asteroids in scene
        foreach (AsteroidController ast in asteroids)
            ast.Split();
        Assert.IsTrue(score != GameManager.score);
        Assert.IsTrue(GameManager.score == 3000);
    }


    [UnityTest]
    public IEnumerator _11ReachingCertainScoreChangesSpaceshipWeapons()
    {
        Object.Instantiate(Prefab_GameManager);
        GameManager.InitializeTestingEnvironment(true, false, false, false, false);
        yield return null;
        SpaceshipController spaceship = Object.FindObjectOfType<SpaceshipController>();
        spaceship.canGetBulletTime = false;
        for (int i = 0; i < 10; i++)
            GameManager.AddAsteroidToScore(0);
        Assert.IsTrue(spaceship != null);
        Assert.IsTrue(spaceship.currentWeapon == SpaceshipController.Weapon.Laser); //weapon changes to laser upon reaching 10000 points
    }

    [UnityTest]
    public IEnumerator _12GameManagerSpawnsCollectibles()
    {
        Object.Instantiate(Prefab_GameManager);
        Object.Instantiate(Prefab_MainCamera);
        GameManager.InitializeTestingEnvironment(false, false, false, true, false);
        GameManager.AddToScore(GameManager.COLLECTIBLE_SPAWN_SCORE_THRESHOLD);
        yield return null;
        BaseCollectible[] collectibles = Object.FindObjectsOfType<BaseCollectible>();
        Assert.IsTrue(collectibles.Length == 1);
    }
}
