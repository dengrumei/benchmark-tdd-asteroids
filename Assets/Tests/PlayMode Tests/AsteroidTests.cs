﻿#pragma warning disable IDE1006 // Naming Styles
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;


public class AsteroidTests : TestTemplate {
    //List of prefabs
    GameObject Prefab_Asteroid => GenericTestMethods.GetPrefab("Base_Asteroid");

    [SetUp]
    public override void Setup()
    {
        base.Setup();
        GameManager.InitializeTestingEnvironment(false, false, false, false, false);
    }

    [Test]
    public void _01AsteroidPrefabExists()
    {
        Assert.NotNull(Prefab_Asteroid);
    }

    [Test]
    public void _02AsteroidPrefabCanBeInstantiated()
    {
        GameObject asteroid = (GameObject)Object.Instantiate(Prefab_Asteroid);
        asteroid.name = "Asteroid";
        Assert.NotNull(GameObject.Find("Asteroid"));
    }

    [Test]
    public void _03AsteroidPrefabHasRequiredComponentTransform()
    {
        Assert.IsNotNull(Prefab_Asteroid.GetComponent<Transform>());
    }

    [Test]
    public void _04AsteroidPrefabHasRequiredComponentCollider()
    {
        Assert.IsNotNull(Prefab_Asteroid.GetComponent<CircleCollider2D>());
    }

    [Test]
    public void _05AsteroidPrefabHasRequiredComponentControllerScript()
    {
        Assert.IsNotNull(Prefab_Asteroid.GetComponent<AsteroidController>());
        //checking if script component has required references 
        Assert.IsNotNull(Prefab_Asteroid.GetComponent<AsteroidController>().asteroidDebris);
    }

    [UnityTest]
    public IEnumerator _06AsteroidGameobjectIsDestroyedOnSplit() //splitting an asteroid should destroy the base asteroid
    {
        GameObject asteroid = (GameObject)Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity);
        asteroid.GetComponent<AsteroidController>().Split();
        yield return null;
        Assert.IsTrue(asteroid == null);                                                        //check that base asteroid is destroyed
    }

    [Test]
    public void _07AsteroidSplitCountCanBeChanged() //splitting an asteroid should destroy the base asteroid
    {
        AsteroidController asteroid = Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity).GetComponent<AsteroidController>();
        asteroid.SetSplitCount(2);
        Assert.IsTrue(asteroid.GetSplitCount() == 2);
    }

    [UnityTest]
    public IEnumerator _08AsteroidCanSplitIntoTwo() // splitting the base asteroid should spawn 2 asteroids 
    {
        GameObject asteroid = (GameObject)Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity);
        asteroid.GetComponent<AsteroidController>().Split();                                    //split base asteroid
        yield return null;
        AsteroidController[] asteroids = Object.FindObjectsOfType<AsteroidController>();        //find all asteroids in scene
        Assert.IsTrue(asteroids.Length == 2);                                                   //there should be 2 asteroids in scene now
    }

    [UnityTest] //this test is maybe not really necessary since splittling is already tested?
    public IEnumerator _09AsteroidCanSplitTwice() // splitting the base asteroid and splitting 2 spawned asteroids again should spawn 4 asteroids
    {
        GameObject asteroid = (GameObject)Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity);
        asteroid.GetComponent<AsteroidController>().Split();                                    //split base asteroid
        yield return null;
        AsteroidController[] asteroids = Object.FindObjectsOfType<AsteroidController>();        //find all asteroids in scene
        foreach(AsteroidController roid in asteroids) roid.Split();                             //split found asteroids
        yield return null;
        asteroids = Object.FindObjectsOfType<AsteroidController>();                             //find all asteroids in scene
        Assert.IsTrue(asteroids.Length == 4);                                                   //there should be 4 asteroids in scene now
    }

    [UnityTest]
    public IEnumerator _10AsteroidCantSplitThrice() // it takes three hits to destroy an asteroid from base size, after 3 hits asteroid should not split anymore
    {
        GameObject asteroid = (GameObject)Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity);
        asteroid.GetComponent<AsteroidController>().Split();                                    //split base asteroid  
        yield return null;
        AsteroidController[] asteroids = Object.FindObjectsOfType<AsteroidController>();        //find all asteroids in scene
        foreach (AsteroidController roid in asteroids) roid.Split();                            //split found asteroids
        yield return null;
        asteroids = Object.FindObjectsOfType<AsteroidController>();                             //find all asteroids in scene
        foreach (AsteroidController roid in asteroids) roid.Split();                            //split found asteroids
        yield return null;
        asteroids = Object.FindObjectsOfType<AsteroidController>();                             //find all asteroids in scene
        Assert.IsTrue(asteroids.Length == 0);                                                   //there should be no asteroids in scene
    }

    [UnityTest]
    public IEnumerator _11AsteroidScaleIsCutInHalfOnSplit() // splitting the asteroid should spawn 2 asteroids at half scale of the split asteroid 
    {
        GameObject asteroid = (GameObject)Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity);
        asteroid.GetComponent<AsteroidController>().Split();                                    //split base asteroid
        yield return null;
        AsteroidController[] asteroids = Object.FindObjectsOfType<AsteroidController>();        //find all asteroids in scene 
        foreach (AsteroidController roid in asteroids)  
            Assert.IsTrue(roid.transform.localScale == new Vector3(0.5f, 0.5f, 0.5f));          //check if split asteroid scale is (0.5, 0.5, 0.5)
    }


    [Test]
    public void _12AsteroidCanMove()
    {
        AsteroidController asteroid = Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity).GetComponent<AsteroidController>();
        asteroid.Move();
        Assert.IsTrue(asteroid.transform.position != Vector3.zero);
    }

    [Test]
    public void _13AsteroidDirectionCanBeChanged()
    {
        AsteroidController asteroid = Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity).GetComponent<AsteroidController>();
        asteroid.SetDirection(Vector2.up);
        Assert.IsTrue(asteroid.GetDirection() == Vector2.up);
    }

    [UnityTest]
    public IEnumerator _14AsteroidMovesAccordingToItsDirectionVector()
    {
        AsteroidController asteroid = Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity).GetComponent<AsteroidController>();
        asteroid.SetDirection(Vector2.up);
        Assert.IsTrue(asteroid.GetDirection() == Vector2.up);
        float t = 0.5f;
        while (t > 0.0f)
        {
            t -= Time.deltaTime;
            yield return null;
        }
        Assert.IsTrue(asteroid.transform.position.x == 0.0f && asteroid.transform.position.y > 0.0f);
    }

    [UnityTest]
    public IEnumerator _15AsteroidIsDestroyedWhenOffsceen()
    {
        AsteroidController asteroid = Object.Instantiate(Prefab_Asteroid, Vector3.right * 100, Quaternion.identity).GetComponent<AsteroidController>();
        yield return null;  
        Assert.IsTrue(asteroid == null);
    }

    [Test]
    public void _16AsteroidPrefabHasRequiredVisual()
    {
        Transform visualChild = Prefab_Asteroid.transform.GetChild(0);
        Assert.IsTrue(visualChild.name == "Visual");
        Assert.IsNotNull(visualChild);
        Assert.IsNotNull(visualChild.GetComponent<MeshRenderer>());
        Assert.IsNotNull(visualChild.GetComponent<MeshRenderer>().sharedMaterials[0]);
        Assert.IsNotNull(visualChild.GetComponent<MeshFilter>());
        Assert.IsNotNull(visualChild.GetComponent<MeshFilter>().sharedMesh);
    }

    [Test]
    public void _17AsteroidPrefabHasAsteroidLayer()
    {
        Assert.IsTrue(Prefab_Asteroid.layer == LayerMask.NameToLayer("Asteroids"));
    }

    [Test]
    public void _18AsteroidPrefabHasRequiredComponentRigidbody()
    {
        Assert.IsNotNull(Prefab_Asteroid.GetComponent<Rigidbody2D>());
        Assert.IsTrue(Prefab_Asteroid.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Dynamic);
        Assert.IsTrue(Prefab_Asteroid.GetComponent<Rigidbody2D>().collisionDetectionMode == CollisionDetectionMode2D.Continuous);
        Assert.IsTrue(Prefab_Asteroid.GetComponent<Rigidbody2D>().interpolation == RigidbodyInterpolation2D.Interpolate);
    }
    
    [UnityTest]
    public IEnumerator _19AsteroidStartsWithARandomRotation()
    {
        AsteroidController asteroid1 = Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity).GetComponent<AsteroidController>();
        AsteroidController asteroid2 = Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity).GetComponent<AsteroidController>();
        AsteroidController asteroid3 = Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity).GetComponent<AsteroidController>();
        yield return null;
        Assert.IsTrue(asteroid1.transform.rotation != asteroid2.transform.rotation && asteroid1.transform.rotation != asteroid3.transform.rotation && asteroid2.transform.rotation != asteroid3.transform.rotation);
    }

    [UnityTest]
    public IEnumerator _20AsteroidSpawnsDebrisWhenDestroyed() 
    {
        GameObject asteroid = (GameObject)Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity);
        asteroid.GetComponent<AsteroidController>().Split();                                    //split base asteroid
        yield return new WaitForFixedUpdate();
        yield return null;
        Assert.IsTrue(asteroid == null);
        Debris[] objects = Object.FindObjectsOfType<Debris>();        //find all spaceship debris in scene
        Assert.IsTrue(objects.Length > 0);
    }

    [UnityTest]
    public IEnumerator _21AsteroidsDontMoveDuringPause()
    {
        AsteroidController asteroid = Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity).GetComponent<AsteroidController>();
        asteroid.SetDirection(Vector2.up);
        Vector3 startPosition = asteroid.transform.position;
        GameManager.IsPaused = true;
        for(int i = 0; i < 20; i++)
            yield return null;
        Assert.IsTrue(asteroid.transform.position == startPosition);
    }
}
