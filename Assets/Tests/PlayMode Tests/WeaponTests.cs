﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class WeaponTests : TestTemplate {
    //List of prefabs
    GameObject Prefab_Spaceship => GenericTestMethods.GetPrefab("Spaceship");
    GameObject Prefab_Asteroid => GenericTestMethods.GetPrefab("Base_Asteroid");
    GameObject Prefab_WeaponLaser => GenericTestMethods.GetPrefab("Weapon_Laser");
    GameObject Prefab_WeaponProjectile => GenericTestMethods.GetPrefab("Weapon_Projectile");

    [SetUp]
    public override void Setup()
    {
        base.Setup();
        GameManager.InitializeTestingEnvironment(false, false, false, false, false);
    }

    [Test]
    public void _01ProjectilePrefabExists()
    {
        Assert.NotNull(Prefab_WeaponProjectile);
    }

    [Test]
    public void _02ProjectilePrefabCanBeInstantiated()
    {
        GameObject projectile = (GameObject)Object.Instantiate(Prefab_WeaponProjectile);
        projectile.name = "Projectile";
        Assert.NotNull(GameObject.Find("Projectile"));
    }

    [Test]
    public void _03ProjectilePrefabHasRequiredComponentTransform()
    {
        Assert.IsNotNull(Prefab_WeaponProjectile.GetComponent<Transform>());
    }

    [Test]
    public void _04ProjectilePrefabHasRequiredComponentCollider()
    {
        Assert.IsNotNull(Prefab_WeaponProjectile.GetComponent<BoxCollider2D>());
        Assert.IsTrue(Prefab_WeaponProjectile.GetComponent<BoxCollider2D>().size == new Vector2(0.2f, 0.2f));
    }

    [Test]
    public void _05ProjectilePrefabHasRequiredComponentControllerScript()
    {
        Assert.IsNotNull(Prefab_WeaponProjectile.GetComponent<ProjectileController>());
    }

    [Test]
    public void _06ProjectilePrefabHasRequiredVisual()
    {
        Transform visualChild = Prefab_WeaponProjectile.transform.GetChild(0);
        Assert.IsTrue(visualChild.name == "Visual");
        Assert.IsNotNull(visualChild);
        Assert.IsNotNull(visualChild.GetComponent<MeshRenderer>());
        Assert.IsNotNull(visualChild.GetComponent<MeshRenderer>().sharedMaterials[0]);
        Assert.IsTrue(visualChild.GetComponent<MeshRenderer>().sharedMaterials[0].shader.name == "Mobile/Particles/Additive"); //shader directory might be changed after upgrade
        Assert.IsNotNull(visualChild.GetComponent<MeshFilter>());
        Assert.IsNotNull(visualChild.GetComponent<MeshFilter>().sharedMesh);
    }

    [Test]
    public void _07ProjectileCanMove()
    {
        ProjectileController projectile = Object.Instantiate(Prefab_WeaponProjectile, Vector3.zero, Quaternion.identity).GetComponent<ProjectileController>();
        projectile.Move();
        Assert.IsTrue(projectile.transform.position != Vector3.zero);
    }

    [Test]
    public void _08ProjectileDirectionCanBeChanged()
    {
        ProjectileController projectile = Object.Instantiate(Prefab_WeaponProjectile, Vector3.zero, Quaternion.identity).GetComponent<ProjectileController>();
        projectile.SetDirection(Vector2.up);
        Assert.IsTrue(projectile.GetDirection() == Vector2.up);
    }

    [UnityTest]
    public IEnumerator _09ProjectileMovesAccordingToItsDirectionVector()
    {
        ProjectileController projectile = Object.Instantiate(Prefab_WeaponProjectile, Vector3.zero, Quaternion.identity).GetComponent<ProjectileController>();
        projectile.SetDirection(Vector2.up);
        Assert.IsTrue(projectile.GetDirection() == Vector2.up);
        float t = 0.5f;
        while (t > 0.0f)
        {
            t -= Time.deltaTime;
            yield return null;
        }
        Assert.IsTrue(projectile.transform.position.x == 0.0f && projectile.transform.position.y > 0.0f);  //check if projectile moves according to given trajectory along the Y axis
    }

    [UnityTest]
    public IEnumerator _10ProjectileRotatesAccordingToItsDirectionVector()
    {
        ProjectileController projectile = Object.Instantiate(Prefab_WeaponProjectile, Vector3.zero, Quaternion.identity).GetComponent<ProjectileController>();
        projectile.SetDirection(new Vector2(0.714f, -0.156f).normalized);
        yield return null;
        Assert.IsTrue((Vector2)projectile.transform.up == projectile.GetDirection());  
    }

    [UnityTest]
    public IEnumerator _11ProjectileIsDestroyedWhenOffsceen()
    {
        ProjectileController projectile = Object.Instantiate(Prefab_WeaponProjectile, Vector3.right * 100, Quaternion.identity).GetComponent<ProjectileController>();
        yield return null;
        Assert.IsTrue(projectile == null);
    }

    [Test]
    public void _12ProjectilePrefabHasRequiredComponentRigidbody()
    {
        Assert.IsNotNull(Prefab_WeaponProjectile.GetComponent<Rigidbody2D>());
        Assert.IsTrue(Prefab_WeaponProjectile.GetComponent<Rigidbody2D>().isKinematic);
        Assert.IsTrue(Prefab_WeaponProjectile.GetComponent<Rigidbody2D>().collisionDetectionMode == CollisionDetectionMode2D.Continuous);
        Assert.IsTrue(Prefab_WeaponProjectile.GetComponent<Rigidbody2D>().interpolation == RigidbodyInterpolation2D.Interpolate);
    }

    [Test]
    public void _13ProjectilePrefabHasProjectileLayer()
    {
        Assert.IsTrue(Prefab_WeaponProjectile.layer == LayerMask.NameToLayer("Projectiles"));
    }

    [UnityTest]
    public IEnumerator _14ProjectileIsDestroyedOnCollisionWithAsteroid()
    {
        GameObject projectile = Object.Instantiate(Prefab_WeaponProjectile, Vector3.zero, Quaternion.identity);
        Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity);
        yield return new WaitForFixedUpdate();
        yield return null;
        Assert.IsTrue(projectile == null);                                       
    }

    [UnityTest]
    public IEnumerator _15ProjectileIgnoresCollisionWithSpaceship()
    {
        GameObject projectile = Object.Instantiate(Prefab_WeaponProjectile, Vector3.zero, Quaternion.identity);
        Object.Instantiate(Prefab_Spaceship, Vector2.zero, Quaternion.identity);
        yield return new WaitForFixedUpdate();
        yield return null;
        Assert.IsTrue(projectile != null);                                   

    }

    [UnityTest]
    public IEnumerator _16ProjectileTriggersAsteroidSplit()
    {
        Object.Instantiate(Prefab_WeaponProjectile, Vector3.zero, Quaternion.identity);
        Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity);
        yield return new WaitForFixedUpdate();
        yield return null;
        AsteroidController[] asteroids = Object.FindObjectsOfType<AsteroidController>();        //find all asteroids in scene
        Assert.IsTrue(asteroids.Length > 1);
     }


    [UnityTest]
    public IEnumerator _17ProjectilesCannotSplitTheSameAsteroidTwice()
    {
        Object.Instantiate(Prefab_WeaponProjectile, Vector3.zero, Quaternion.identity);
        Object.Instantiate(Prefab_WeaponProjectile, Vector3.zero, Quaternion.identity);
        Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity);
        yield return new WaitForFixedUpdate();
        yield return null;
        AsteroidController[] asteroids = Object.FindObjectsOfType<AsteroidController>();        //find all asteroids in scene
        Assert.IsTrue(asteroids.Length == 2);
    }


    [UnityTest]
    public IEnumerator _18ProjectilesDontMoveDuringPause()
    {
        ProjectileController projectile = Object.Instantiate(Prefab_WeaponProjectile, Vector3.zero, Quaternion.identity).GetComponent<ProjectileController>();
        projectile.SetDirection(Vector2.up);
        Vector3 startPosition = projectile.transform.position;
        GameManager.IsPaused = true;
        for (int i = 0; i < 20; i++)
            yield return null;
        Assert.IsTrue(projectile.transform.position == startPosition);
    }



    [UnityTest]
    public IEnumerator _19LaserFiresAndIsChildOfSpaceship()
    {
        SpaceshipController spaceship = Object.Instantiate(Prefab_Spaceship, Vector2.zero, Quaternion.identity).GetComponent<SpaceshipController>();
        spaceship.currentWeapon = SpaceshipController.Weapon.Laser;
        spaceship.Shoot();
        yield return null;
        LaserController laser = Object.FindObjectOfType<LaserController>();
        Assert.NotNull(laser);      //laser exists
        Assert.IsTrue(laser.transform.parent == spaceship.transform); //laser is child of spaceship
        Assert.IsTrue(laser.transform.up == spaceship.transform.up);
    }

    [UnityTest]
    public IEnumerator _20LaserColliderBoundsChangeWithScale()
    {
        BoxCollider2D laser = Object.Instantiate(Prefab_WeaponLaser, Vector3.zero, Quaternion.identity).GetComponent<BoxCollider2D>();
        Assert.NotNull(laser);      //laser has box collider
        yield return null;
        if (laser != null)
        {
            Assert.That(laser.bounds.size.y, Is.EqualTo(laser.transform.localScale.y / 2.0f).Within(Mathf.Epsilon));      //collider bound length is always half the size of laser y localscale
            Assert.That(laser.bounds.size.x, Is.EqualTo(0.2f).Within(Mathf.Epsilon));      //collider bound width is  always constant
        }
        yield return null;
    }

    [UnityTest]
    public IEnumerator _21LaserIsDestroyedAfterStoppingFiring()
    {
        GameObject laser = Object.Instantiate(Prefab_WeaponLaser, Vector3.zero, Quaternion.identity);
        Assert.NotNull(laser);      //laser exists at the start of firing
        yield return new WaitForSeconds(1.5f);
        Assert.IsTrue(laser == null);      //laser is destroyed after a certain duration
    }

    [UnityTest]
    public IEnumerator _22LaserCanDestroyDistantObjects()
    {
        GameObject laser = Object.Instantiate(Prefab_WeaponLaser, Vector3.zero, Quaternion.identity);
        GameObject asteroid = Object.Instantiate(Prefab_Asteroid, Vector3.up * 12.0f, Quaternion.identity);
        yield return new WaitForSeconds(1.0f);
        Assert.IsTrue(laser != null);
        Assert.IsTrue(asteroid == null);
    }

    [UnityTest]
    public IEnumerator _23OnlyOneInstanceOfLaserCanBeFiredAtOnce()
    {
        SpaceshipController spaceship = Object.Instantiate(Prefab_Spaceship, Vector2.zero, Quaternion.identity).GetComponent<SpaceshipController>();
        spaceship.currentWeapon = SpaceshipController.Weapon.Laser;
        spaceship.Shoot();
        spaceship.Shoot();
        spaceship.Shoot();
        spaceship.Shoot();
        yield return null;
        LaserController[] lasers = Object.FindObjectsOfType<LaserController>();
        Assert.IsTrue(lasers.Length == 1);     
    }
}
