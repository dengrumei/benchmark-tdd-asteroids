﻿#pragma warning disable IDE1006 // Naming Styles
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FXTests : TestTemplate {
    //List of prefabs
    GameObject Prefab_SpaceshipDebris => GenericTestMethods.GetPrefab("Spaceship_Debris");
    GameObject Prefab_AsteroidDebris => GenericTestMethods.GetPrefab("Asteroid_Debris");

    [SetUp]
    public override void Setup()
    {
        base.Setup();
        GameManager.InitializeTestingEnvironment(false, false, true, false, false);
    }

    [Test]
    public void _01DebrisPrefabsExists()
    {
        Assert.NotNull(Prefab_SpaceshipDebris);
        Assert.NotNull(Prefab_SpaceshipDebris.GetComponent<Debris>());

        Assert.NotNull(Prefab_AsteroidDebris);
        Assert.NotNull(Prefab_AsteroidDebris.GetComponent<Debris>());
    }


    [UnityTest]
    public IEnumerator _02DebrisFragmentsHaveVelocityOnStart()
    {
        GameObject debris = Object.Instantiate(Prefab_SpaceshipDebris, Vector3.zero, Quaternion.identity);
        yield return new WaitForFixedUpdate();
        yield return null;
        foreach(Rigidbody2D fragment in debris.GetComponentsInChildren<Rigidbody2D>())
        {
            Assert.IsTrue(fragment.velocity != Vector2.zero);
        }
    }


    [UnityTest]
    public IEnumerator _03DebrisFragmentsAreDestroyedAfterSeconds()
    {
        GameObject debris = Object.Instantiate(Prefab_SpaceshipDebris, Vector3.zero, Quaternion.identity);
        yield return new WaitForFixedUpdate();
        yield return null;
        yield return new WaitForSeconds(1.0f); //should be destroyed after 1 sec
        Assert.IsTrue(debris == null);
        Assert.IsTrue(Object.FindObjectsOfType<Rigidbody2D>().Length == 0);
    }


    [UnityTest]
    public IEnumerator _04ExplosionParticlesAreSpawnedWithDebris()
    {
        GameObject debris = Object.Instantiate(Prefab_SpaceshipDebris, Vector3.zero, Quaternion.identity);
        yield return null;
        Assert.IsTrue(Object.FindObjectOfType<ParticleSystem>() != null); //PS exists
        ParticleSystem particleSystem = Object.FindObjectOfType<ParticleSystem>().transform.root.GetComponent<ParticleSystem>();
        Assert.IsTrue(particleSystem != null); //PS exists
        yield return new WaitForSeconds(particleSystem.main.duration);
        Assert.IsTrue(particleSystem == null); //PS is destroyed after duration
    }

    [UnityTest]
    public IEnumerator _05ExplosionParticlesActivateChildParticlesAndSubEmmitersSuccessfully()
    {
        ParticleSystem explosion = Object.Instantiate(Prefab_SpaceshipDebris.GetComponent<Debris>().explosionParticles, Vector3.zero, Quaternion.identity).GetComponent<ParticleSystem>();
        Assert.IsTrue(explosion != null); //PS exists
        Assert.IsTrue(explosion.transform.GetChild(0).GetComponent<ParticleSystem>() != null); //Child PS exists
        Assert.IsTrue(explosion.transform.GetChild(1).GetComponent<ParticleSystem>() != null); //Child PS exists
        Assert.IsTrue(explosion.transform.GetChild(2).GetComponent<ParticleSystem>() != null); //Child PS exists
        yield return null;
        Assert.IsTrue(explosion.particleCount == 50); //main explosion PS burst emits 50 particles on the first frame
        Assert.IsTrue(explosion.transform.GetChild(0).GetComponent<ParticleSystem>().particleCount == 1); //Glow emits 1 particle on the first frame
        Assert.IsTrue(explosion.transform.GetChild(1).GetComponent<ParticleSystem>().particleCount == 2); //shockwave emits 2 particle on the first frame
        Assert.IsTrue(explosion.subEmitters.GetSubEmitterSystem(0) != null); // subemitter exists, indexOutOfRange if subemitter cannot be found
        yield return new WaitForSeconds(0.2f);
        Assert.IsTrue(explosion.subEmitters.GetSubEmitterSystem(0).particleCount > 0); //subemitter emits particles over time
    }
}
