﻿#pragma warning disable IDE1006 // Naming Styles
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class SpaceshipTests : TestTemplate{
    //List of prefabs
    GameObject Prefab_Spaceship => GenericTestMethods.GetPrefab("Spaceship");
    GameObject Prefab_Asteroid => GenericTestMethods.GetPrefab("Base_Asteroid");
    GameObject Prefab_MainCamera => GenericTestMethods.GetPrefab("Gameplay_Camera");    
    GameObject Prefab_Base_Collectible => GenericTestMethods.GetPrefab("Base_Collectible");


    [SetUp]
    public override void Setup()
    {
        base.Setup();
        GameManager.InitializeTestingEnvironment(false, false, true, false, false);
        Object.Instantiate(Prefab_MainCamera);
    }

    [Test]
    public void _01SpaceshipPrefabExists() {
        Assert.NotNull(Prefab_Spaceship);
    }

    [Test]
    public void _02SpaceshipPrefabCanBeInstantiated()
    {
        GameObject spaceship = (GameObject)Object.Instantiate(Prefab_Spaceship);
        spaceship.name = "Spaceship";
        Assert.NotNull(GameObject.Find("Spaceship"));
    }

    [Test]
    public void _03SpaceshipPrefabHasRequiredComponentTransform()
    {
        Assert.IsNotNull(Prefab_Spaceship.GetComponent<Transform>());
    }

    [Test]
    public void _04SpaceshipPrefabHasRequiredComponentCollider()
    {
        Assert.IsNotNull(Prefab_Spaceship.GetComponent<PolygonCollider2D>());
    }

    [Test]
    public void _05SpaceshipPrefabHasRequiredComponentControllerScript()
    {
        Assert.IsNotNull(Prefab_Spaceship.GetComponent<SpaceshipController>());
        //checking if script component has required references 
        Assert.IsNotNull(Prefab_Spaceship.GetComponent<SpaceshipController>().spaceshipDebris);
        Assert.IsNotNull(Prefab_Spaceship.GetComponent<SpaceshipController>().weaponList);
    }


    [Test]
    public void _06SpaceshipPrefabHasRequiredVisual()
    {
        Transform visualChild = Prefab_Spaceship.transform.GetChild(0);
        Assert.IsTrue(visualChild.name == "Visual");
        Assert.IsNotNull(visualChild);
        Assert.IsNotNull(visualChild.GetComponent<MeshRenderer>());
        Assert.IsNotNull(visualChild.GetComponent<MeshRenderer>().sharedMaterials[0]);
        Assert.IsNotNull(visualChild.GetComponent<MeshRenderer>().sharedMaterials[1]);
        Assert.IsNotNull(visualChild.GetComponent<MeshFilter>());
        Assert.IsNotNull(visualChild.GetComponent<MeshFilter>().sharedMesh);
    }

    [Test]
    public void _07SpaceshipPrefabHasRequiredComponentRigidbody()
    {
        Assert.IsNotNull(Prefab_Spaceship.GetComponent<Rigidbody2D>());
        Assert.IsTrue(Prefab_Spaceship.GetComponent<Rigidbody2D>().isKinematic);
        Assert.IsTrue(Prefab_Spaceship.GetComponent<Rigidbody2D>().collisionDetectionMode == CollisionDetectionMode2D.Continuous);
        Assert.IsTrue(Prefab_Spaceship.GetComponent<Rigidbody2D>().interpolation == RigidbodyInterpolation2D.Interpolate);
    }

    [Test]
    public void _08SpaceshipPrefabHasAsteroidLayer()
    {
        Assert.IsTrue(Prefab_Spaceship.layer == LayerMask.NameToLayer("Spaceship"));
    }


    [UnityTest]
    public IEnumerator _09SpaceshipIsDestroyedOnCollisionWithAsteroid()
    {
        GameObject spaceship = Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.identity);
        Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity);
        yield return new WaitForFixedUpdate();
        yield return null;
        Assert.IsTrue(spaceship == null);
    }

    [UnityTest]
    public IEnumerator _10SpaceshipTriggersAsteroidSplit()
    {
        Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.identity);
        Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity);
        yield return new WaitForFixedUpdate();
        yield return null;
        AsteroidController[] asteroids = Object.FindObjectsOfType<AsteroidController>();        //find all asteroids in scene
        Assert.IsTrue(asteroids.Length > 1);
    }



    [Test]
    public void _11SpaceshipCanMove()
    {
        SpaceshipController spaceship = Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.identity).GetComponent<SpaceshipController>();
        spaceship.direction = Vector2.up;
        spaceship.Move();
        Assert.IsTrue(spaceship.transform.position != Vector3.zero);
    }

    /* badly written test
    [UnityTest]
    public IEnumerator _12SpaceshipRotationCanBeChanged()
    {
        SpaceshipController spaceship = Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.identity).GetComponent<SpaceshipController>();
        spaceship.transform.eulerAngles = new Vector3(0.0f, 0.0f, 180.0f);

        float startingRotation = spaceship.transform.eulerAngles.z;
        for (float i = 0; i < Time.deltaTime * 10;)
        {
            spaceship.Turn(1.0f); //turn right
            i += Time.deltaTime;
        }
        yield return null;
        Assert.IsTrue(spaceship.transform.eulerAngles.z < startingRotation);

        startingRotation = spaceship.transform.eulerAngles.z;
        for (float i = 0; i < Time.deltaTime * 10;)
        {
            spaceship.Turn(-1.0f); //turn right
            i += Time.deltaTime;
        }
        yield return null;
        Assert.IsTrue(spaceship.transform.eulerAngles.z > startingRotation);
    }
    */

    [Test]
    public void _13SpaceshipMovesAccordingToItsDirectionVector()
    {
        SpaceshipController spaceship = Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.Euler(0.0f, 0.0f, 0.0f)).GetComponent<SpaceshipController>();
        spaceship.Thrust(1.0f);
        spaceship.Move();
        Assert.IsTrue(spaceship.transform.position.y >= 0.0f && spaceship.transform.position.x == 0.0f);
    }

    /*
    [UnityTest]
    public IEnumerator _14SpaceshipVisualRotatesWhenTurning() //BAD TEST, WILL REDO LATER
    {
        SpaceshipController spaceship = Object.Instantiate(spaceshipPrefab, Vector3.zero, Quaternion.Euler(0.0f, 0.0f, 0.0f)).GetComponent<SpaceshipController>();
        float startRotation = spaceship.transform.GetChild(0).eulerAngles.z;
        for (int i = 0; i < 10; i++)
        {
            spaceship.Turn(1.0f);
            yield return null;
        }
        yield return null;
        Assert.IsTrue(spaceship.transform.GetChild(0).eulerAngles.z != startRotation); 
    }
    */

    [UnityTest]
    public IEnumerator _15SpaceshipIsWarpedToTheOtherSideOfTheScreenAfterMovingOffscreen()
    {
        SpaceshipController spaceship = Object.Instantiate(Prefab_Spaceship, Vector2.right * 100.0f, Quaternion.identity).GetComponent<SpaceshipController>();
        yield return null;
        Assert.IsTrue(spaceship.transform.position.x < 0.0f);
        spaceship.transform.position = Vector2.left * 100.0f;
        yield return null;
        Assert.IsTrue(spaceship.transform.position.x > 0.0f);
        spaceship.transform.position = Vector2.up * 100.0f;
        yield return null;
        Assert.IsTrue(spaceship.transform.position.y < 0.0f);
        spaceship.transform.position = Vector2.down * 100.0f;
        yield return null;
        Assert.IsTrue(spaceship.transform.position.y > 0.0f);
    }

    [Test]
    public void _16SpaceshipCanFireProjectiles()
    {
        SpaceshipController spaceship = Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.Euler(0.0f, 0.0f, 0.0f)).GetComponent<SpaceshipController>();
        spaceship.Shoot();
        ProjectileController projectile = Object.FindObjectOfType<ProjectileController>();        //find all asteroids in scene
        Assert.IsTrue(projectile != null);
    }

    [UnityTest]
    public IEnumerator _16SpaceshipSpawnsDebrisWhenDestroyed()
    {
        GameObject spaceship = Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.identity);
        Object.Instantiate(Prefab_Asteroid, Vector3.zero, Quaternion.identity);
        yield return new WaitForFixedUpdate();
        yield return null;
        Assert.IsTrue(spaceship == null);
        Debris[] objects = Object.FindObjectsOfType<Debris>();        //find all spaceship debris in scene
        Assert.IsTrue(objects.Length > 0);
    }

    [UnityTest]
    public IEnumerator _17SpaceshipEngineEmitsParticles()
    {
        GameObject spaceship = Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.identity);
        Assert.IsTrue(spaceship.transform.GetChild(0).GetChild(0) != null);                                 //emitter particle system child exists
        Assert.IsTrue(spaceship.transform.GetChild(0).GetChild(0).GetComponent<EngineTrail>() != null);     //EngineTrail component exists
        ParticleSystem ps = spaceship.transform.GetChild(0).GetChild(0).GetComponent<ParticleSystem>();
        Assert.IsTrue(ps != null);                                                                          //ParticleSystem component exists
        Assert.IsTrue(ps.particleCount == 0);                                                               //No particles are emitted on instantiate
        yield return null;
        Assert.IsTrue(ps.particleCount > 0);                                                                //particles start emitting
    }

    [UnityTest]
    public IEnumerator _18SpaceshipEngineParticlesAreClearedAfterWarp()
    {
        GameObject spaceship = Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.identity);
        ParticleSystem ps = spaceship.transform.GetChild(0).GetChild(0).GetComponent<ParticleSystem>();
        yield return null;                                      //wait for particles to spawn
        spaceship.transform.position = Vector2.left * 100.0f;
        yield return null;   
        Assert.IsTrue(ps.particleCount == 0);                   //see if particles are cleared
    }

    /*
    [UnityTest]
    public IEnumerator _19SpaceshipEngineCausesParticleBurstOnMovementStart()
    {
    }

    [UnityTest]
    public IEnumerator _20SpaceshipEngineSpawnsMoreParticlesWhileMoving()
    {
    }
    */


    [UnityTest]
    public IEnumerator _19SpaceshipDoesntMoveDuringPause()
    {
        SpaceshipController spaceship = Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.identity).GetComponent<SpaceshipController>();
        spaceship.direction = Vector2.up;
        Vector3 startPosition = spaceship.transform.position;
        GameManager.IsPaused = true;
        for (int i = 0; i < 20; i++)
            yield return null;
        Assert.IsTrue(spaceship.transform.position == startPosition);
    }


    [Test]
    public void _20SpaceshipPrefabHasRequiredComponentAnimator()
    {
        Assert.IsNotNull(Prefab_Spaceship.transform.GetChild(0).GetComponent<Animator>());
        Assert.IsNotNull(Prefab_Spaceship.transform.GetChild(0).GetComponent<Animator>().runtimeAnimatorController != null);
        Assert.IsTrue(Prefab_Spaceship.transform.GetChild(0).GetComponent<Animator>().runtimeAnimatorController.name == "SpaceshipAnimator");
        Assert.IsTrue(Prefab_Spaceship.transform.GetChild(0).GetComponent<Animator>().cullingMode == AnimatorCullingMode.AlwaysAnimate);
        Assert.IsTrue(Prefab_Spaceship.transform.GetChild(0).GetComponent<Animator>().updateMode == AnimatorUpdateMode.Normal);
    }

    [UnityTest]
    public IEnumerator _21SpaceshipAnimatorPlaysSpawnAnimationOnSpawn()
    {
        Animator animator = Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.identity).transform.GetChild(0).GetComponent<Animator>();
        yield return null;
        Assert.IsNotNull(animator);
        Assert.IsTrue(animator.GetCurrentAnimatorStateInfo(0).IsName("SpaceshipSpawn"));    //spawn animation plays
        float timeout = 5.0f;
        while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime <= 1.0f)
        {
            yield return null;
            timeout -= Time.deltaTime;
            if (timeout < 0.0f)
                Assert.IsTrue(false);                                                       //animation never finishes in time
        }
        Assert.IsTrue(animator.transform.localScale == Vector3.one);                        //spawn animation is fully finished
    }

    [Test]
    public void _22SpaceshipWeaponListContainsData()
    {
        WeaponList weaponList = Prefab_Spaceship.GetComponent<SpaceshipController>().weaponList;
        Assert.IsNotNull(weaponList);
        Assert.IsTrue(weaponList.weapons.Count == 2); // list should have 2 weapons (so far)
        foreach(WeaponList.Weapon weapon in weaponList.weapons)
        {
            Assert.IsTrue(weapon.weaponName.Length != 0);
            Assert.IsNotNull(weapon.weaponPrefab);
        }
    }

    [Test]
    public void _23SpaceshipHasGravitationalPull()
    {
        SpaceshipController spaceship = Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.identity).GetComponent<SpaceshipController>();
        Assert.NotNull(spaceship.transform.GetChild(1));
        Rigidbody rigidbody = spaceship.transform.GetChild(1).GetComponent<Rigidbody>();
        GravitationalPull gravitationalPull = spaceship.transform.GetChild(1).GetComponent<GravitationalPull>();
        Assert.NotNull(rigidbody);
        Assert.NotNull(gravitationalPull);
        Assert.IsFalse(rigidbody.useGravity, "Gravity should be disabled");
        Assert.IsTrue(rigidbody.interpolation == RigidbodyInterpolation.None, "Interpolation should be RigidbodyInterpolation.None");
        Assert.IsTrue(rigidbody.collisionDetectionMode == CollisionDetectionMode.Discrete, "collisionDetectionMode should be CollisionDetectionMode.Discrete");
    }

    [UnityTest]
    public IEnumerator _24SpaceshipGravitationalPullRigidbodyStaysInPlace()
    {
        SpaceshipController spaceship = Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.identity).GetComponent<SpaceshipController>();
        Rigidbody rigidbody = spaceship.transform.GetChild(1).GetComponent<Rigidbody>();
        default(Assert).VectorsAreEqual(spaceship.transform.position, rigidbody.position, 0.001f, "Initial Rigidbody position (" + rigidbody.position + ") != transform.position (" + spaceship.transform.position + "). Child rigidbody should move together with the parent transform.");
        for (int i = 0; i < 15; i++)
        {
            spaceship.Thrust(new Vector2(Mathf.Sin(Time.unscaledTime * 13.37f), Mathf.Cos(Time.unscaledTime * 69.0f)) * 420.0f);
            yield return null;
            default(Assert).VectorsAreEqual(spaceship.transform.position, rigidbody.position, 0.001f, "Rigidbody position after moving (" + rigidbody.position + ") != transform.position (" + spaceship.transform.position + "). Child rigidbody should move together with the parent transform.");
            default(Assert).VectorsAreEqual(spaceship.transform.eulerAngles, rigidbody.rotation.eulerAngles, 0.001f, "Rigidbody rotation (" + rigidbody.rotation.eulerAngles + ") != " + spaceship.transform.rotation.eulerAngles + ". This rigidbody should not rotate with the parent transform.");
        }
    }

    [UnityTest]
    public IEnumerator _25SpaceshipGravitationalPullAttractsCollectibles()
    {
        for (int j = 0; j < 10; j++)
        {
            SpaceshipController spaceship = Object.Instantiate(Prefab_Spaceship, Vector3.zero, Quaternion.identity).GetComponent<SpaceshipController>();
            GameObject collectible = Object.Instantiate(Prefab_Base_Collectible, new Vector3(Mathf.Sin(0.2f * (j % 5) * Mathf.PI * 2), Mathf.Cos(0.2f * (j % 5) * Mathf.PI * 2), Mathf.Sin(0.67f * j * Mathf.PI * 2)).normalized, Quaternion.identity);
            Rigidbody rigidbody = collectible.GetComponent<Rigidbody>();
            Assert.NotNull(rigidbody);
            Vector3 initialTransformPosition = collectible.transform.position;
            Vector3 initialRigidbodyPosition = rigidbody.position;
            default(Assert).VectorsAreEqual(initialTransformPosition, initialRigidbodyPosition, 0.001f, "Rigidbody position (" + initialRigidbodyPosition + ") != transform.position (" + initialTransformPosition + ").");
            yield return GenericTestMethods.SkipPhysicsFrames(2);
            Vector3 velocity = rigidbody.velocity.normalized;
            Vector3 secondTransformPosition = collectible.transform.position;
            Vector3 secondRigidbodyPosition = rigidbody.position;
            default(Assert).VectorsAreEqual(secondTransformPosition, secondRigidbodyPosition, 0.001f, "Rigidbody position (" + secondRigidbodyPosition + ") != transform.position (" + secondTransformPosition + ").");
            Assert.IsTrue((secondTransformPosition - initialTransformPosition).magnitude > 0.0f, "Collectible transform is not moving.)");
            Assert.IsTrue((secondRigidbodyPosition - initialRigidbodyPosition).magnitude > 0.0f, "Collectible rigidbody is not moving.)");

            Vector3 transformMovementDirection = (secondTransformPosition - initialTransformPosition).normalized;
            Vector3 rigidbodyMovementDirection = (secondRigidbodyPosition - initialRigidbodyPosition).normalized;
            float transformDotProduct = Vector3.Dot((spaceship.transform.position - initialTransformPosition).normalized, transformMovementDirection);
            float rigidbodyDotProduct = Vector3.Dot((spaceship.transform.position - initialRigidbodyPosition).normalized, rigidbodyMovementDirection);
            float velocityDotProduct = Vector3.Dot((spaceship.transform.position - initialRigidbodyPosition).normalized, velocity);

            Assert.IsTrue(transformDotProduct > 0.0f, "Collectible transform is moving in the wrong direction. Dot product: (" + transformDotProduct + ")");
            Assert.IsTrue(rigidbodyDotProduct > 0.0f, "Collectible rigidbody is moving in the wrong direction. Dot product: (" + rigidbodyDotProduct + ")");
            Assert.IsTrue(velocityDotProduct > 0.0f, "Collectible velocity is pointing in the wrong direction. Dot product: (" + velocityDotProduct + ")");

            Assert.IsTrue(Vector2.Dot(transformMovementDirection, rigidbodyMovementDirection) > 0.9f, "Transform movement direction and Rigidbody movement direction vector mismatch. Dot product: (" + Vector2.Dot(transformMovementDirection, rigidbodyMovementDirection) + ", expected at least >0.9f");
            Assert.IsTrue(Vector2.Dot(transformMovementDirection, velocity) > 0.9f, "Transform movement direction and Rigidbody velocity vector mismatch. Dot product: (" + Vector2.Dot(transformMovementDirection, velocity.normalized) + "), expected at least >0.9f");
            Assert.IsTrue(Vector2.Dot(rigidbodyMovementDirection, velocity) > 0.9f, "Rigidbody movement direction and Rigidbody velocity vector mismatch. Dot product: (" + Vector2.Dot(rigidbodyMovementDirection, velocity.normalized) + "), expected at least >0.9f");

            Teardown();
            Setup();
        }
    }
}
