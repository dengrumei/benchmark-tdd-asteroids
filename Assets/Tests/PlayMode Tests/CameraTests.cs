﻿#pragma warning disable IDE1006 // Naming Styles
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.SceneManagement;

public class CameraTests : TestTemplate {
    //List of prefabs
    GameObject Prefab_MainCamera => GenericTestMethods.GetPrefab("Gameplay_Camera");

    [SetUp]
    public override void Setup()
    {
        base.Setup();
        GameManager.InitializeTestingEnvironment(true, true, true, false, false);
    }

    [Test]
    public void _00CameraPrefabExists()
    {
        Assert.NotNull(Prefab_MainCamera);
    }

    [Test]
    public void _01CameraPrefabHasRequiredComponents()
    {
        Assert.IsTrue(Prefab_MainCamera.GetComponent<Camera>().clearFlags == CameraClearFlags.Skybox);
        Assert.IsTrue(Prefab_MainCamera.GetComponent<Camera>().orthographic);
    }
    
    [UnityTest]
    public IEnumerator _02CameraExistsInScene()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_SCENE_NAME);
        yield return null;
        Camera[] cameras = Object.FindObjectsOfType<Camera>();
        bool mainCameraExists = false;
        bool actionCameraExists = false;
        foreach(Camera camera in cameras)
        {
            mainCameraExists |= camera.GetComponent<CameraController>();
            actionCameraExists |= camera.GetComponent<ActionCamera>();
        }
        Assert.IsTrue(mainCameraExists, "Could not find CameraController in the Gameplay scene.");
        Assert.IsTrue(actionCameraExists, "Could not find ActionCamera in the Gameplay scene.");
    }

    [UnityTest]
    public IEnumerator _03SkyboxMaterialExists()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_SCENE_NAME);
        yield return null;              //skybox isn't initialized immediately after loading the scene? have to skip a frame to get the correct material
        Assert.NotNull(RenderSettings.skybox.mainTexture);
    }
    
    [UnityTest]
    public IEnumerator _04SkyboxReactsToMovement()
    {
        SceneManager.LoadScene(TestConstants.GAMEPLAY_SCENE_NAME);
        GameManager.updateEnabled = false;
        yield return null;
        Vector2 offset1 = RenderSettings.skybox.GetTextureOffset("_MainTex");
        GameManager.spaceship.transform.position = new Vector3(2.0f, 0.0f);
        yield return GenericTestMethods.SkipFrames(4, 0.1f);
        Vector2 offset2 = RenderSettings.skybox.GetTextureOffset("_MainTex");
        Assert.IsTrue(offset1 != offset2);
        GameManager.spaceship.transform.position = new Vector3(0.0f, 2.0f);
        yield return GenericTestMethods.SkipFrames(4, 0.1f);
        Vector2 offset3 = RenderSettings.skybox.GetTextureOffset("_MainTex");
        Assert.IsTrue(offset3 != offset2 && offset3 != offset1);
    }

    [UnityTest]
    public IEnumerator _05ScreenShakeMovesCamera()  //extremely low chance of failing due to Random.range use
    {
        CameraController cam = Object.Instantiate(Prefab_MainCamera).GetComponent<CameraController>();
        Vector3 camPos = cam.transform.position;
        cam.ScreenShake(10.0f, 1.0f);
        for (int i = 0; i < 3; i++)
        {
            Assert.IsTrue(camPos != cam.transform.position);    //check that camera moves every frame
            camPos = cam.transform.position;
            yield return null;            
        }
    }


    [UnityTest]
    public IEnumerator _06CameraPositionIsResetAfterScreenShake()
    {
        CameraController cam = Object.Instantiate(Prefab_MainCamera).GetComponent<CameraController>();
        Vector3 camPos = cam.transform.position;
        yield return null;
        yield return cam.Shake(1.0f, 0.1f);
        Assert.IsTrue(camPos == cam.transform.position);    //check that camera position returns to what it was before screen shake
        yield return null;

        cam.ScreenShake(1.0f, 0.1f);
        yield return null;
        yield return cam.Shake(1.0f, 0.1f);
        Assert.IsTrue(camPos == cam.transform.position);    //check that multiple instances of screen shake does not break this functionality 
    }
}
