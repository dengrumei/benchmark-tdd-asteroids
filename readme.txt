Use 'master-2019.3' branch with 2019.3 and newer Unity releases

To run build tests on Windows: EDITOR_PATH -projectPath PROJECT_PATH -runTests -testPlatform editmode -buildPlatform Windows -scriptingBackend Mono2x
To run build tests on macOS: EDITOR_PATH -projectPath PROJECT_PATH -runTests -testPlatform editmode -buildPlatform macOS -scriptingBackend Mono2x
To run build tests on Linux: EDITOR_PATH -projectPath PROJECT_PATH -runTests -testPlatform editmode -buildPlatform Linux -scriptingBackend Mono2x
To run build tests on Android: EDITOR_PATH -projectPath PROJECT_PATH -runTests -testPlatform editmode -buildPlatform Android -scriptingBackend Mono2x
To run build tests on iOS: EDITOR_PATH -projectPath PROJECT_PATH -runTests -testPlatform editmode -buildPlatform iOS -scriptingBackend IL2CPP
To run build tests on WebGL: EDITOR_PATH -projectPath PROJECT_PATH -runTests -testPlatform editmode -buildPlatform WebGL -scriptingBackend Mono2x
